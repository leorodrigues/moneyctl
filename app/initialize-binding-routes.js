const loadBindingById =
    require('../routes/contexting/load-binding-by-id');

const loadBindingAccountsByToken =
    require('../routes/contexting/load-binding-accounts-by-token');

const askBindingDetails =
    require('../routes/inquiries/ask-binding-details');

const deleteBindingById =
    require('../routes/removal/delete-binding-by-id');

const postBinding =
    require('../routes/saving/post-binding');

const getAllBindings =
    require('../routes/retrieval/get-all-bindings');

const TRANSF_PATTERN = '/from/@:draweeAccountToken/to/@:assigneeAccountToken';

module.exports = class InitializeBindingRoutes {
    invoke(commander, inquirer) {
        const askBindingQuestions = askBindingDetails(inquirer);

        commander.post(`/binding${TRANSF_PATTERN}`,
            loadBindingAccountsByToken, askBindingQuestions, postBinding);

        commander.delete('/binding/:bindingId(\\d+)', deleteBindingById);

        commander.get('/binding/:bindingId(\\d+)', loadBindingById);

        commander.get('/binding', getAllBindings);
    }
};
