
const getAllRoutes =
    require('../routes/retrieval/get-all-routes');

const deleteCollectionByName =
    require('../routes/removal/delete-collection-by-name');

const putInitialState =
    require('../routes/saving/put-initial-state');

const postAccount =
    require('../routes/saving/post-account');

const askInitialStateConfirmation =
    require('../routes/inquiries/ask-initial-state-confirmation');

const askAccountDetails =
    require('../routes/inquiries/ask-account-details');

module.exports = class InitializeSystemRoutes {
    invoke(commander, inquirer) {
        const askInitialStateQuestions = askInitialStateConfirmation(inquirer);
        const askRootAccountQuestions =  askAccountDetails(inquirer, {
            title: 'Please provide information about the default account'
        });

        commander.delete('/system/db/collection/:collectionName',
            deleteCollectionByName);

        commander.put('/system/db/initial-state',
            askInitialStateQuestions,
            putInitialState,
            askRootAccountQuestions,
            postAccount);

        commander.get('/console/routes',
            getAllRoutes);
    }
};
