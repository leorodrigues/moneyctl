const loadAccountByToken =
    require('../routes/contexting/load-account-by-token');

const loadAccountById =
    require('../routes/contexting/load-account-by-id');

const deleteAccountById =
    require('../routes/removal/delete-account-by-id');

const getAllAccounts =
    require('../routes/retrieval/get-all-accounts');

const postAccount =
    require('../routes/saving/post-account');

const putAccountById =
    require('../routes/saving/put-account-by-id');

const askAccountDetails =
    require('../routes/inquiries/ask-account-details');

const postBill =
    require('../routes/saving/post-bill');

module.exports = class InitializeAccountRoutes {
    invoke(commander, inquirer) {

        const askAccountQuestions = askAccountDetails(inquirer);
    
        commander.post('/account/@:accountToken/bill',
            loadAccountByToken, postBill);

        // commander.get('/account/@:accountToken/bill',
        //     getAllBillsByAccountToken);

        commander.delete('/account/:accountId(\\d+)',
            deleteAccountById);

        commander.put('/account/:accountId(\\d+)',
            loadAccountById, askAccountQuestions, putAccountById);

        commander.get('/account/:accountId(\\d+)',
            loadAccountById);

        commander.post('/account',
            askAccountQuestions, postAccount);

        commander.get('/account', getAllAccounts);
    }
};