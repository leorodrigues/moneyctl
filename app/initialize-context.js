
const { ATM, Factory } = require('../engines/model');
const database = require('../db');

const configureContext =
    require('../routes/contexting/configure-context');

module.exports = class InitializeContext {
    invoke(commander) {
        const atm = new ATM(new Factory(), database);
        commander.beforeEach(configureContext(atm));
        return atm;
    }
};
