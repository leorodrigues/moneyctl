
const handleFragmentDataForDisplay =
    require('../routes/presentation/handle-fragment-data-for-display');

const renderFragmentStream =
    require('../routes/presentation/render-fragment-stream');

module.exports = class InitializeGlobalHandlers {
    invoke(commander, inquirer, presentationStreamer) {
        const fragmentDataForDisplay =
            handleFragmentDataForDisplay(presentationStreamer);
        commander.afterEach(fragmentDataForDisplay, renderFragmentStream);
    }
};