const getAllTransfers =
    require('../routes/retrieval/get-all-transfers');

const deleteTransferById =
    require('../routes/removal/delete-transfer-by-id');

const postTransfer =
    require('../routes/saving/post-transfer');

const loadTransferById =
    require('../routes/contexting/load-transfer-by-id');

const askTransferDetails =
    require('../routes/inquiries/ask-transfer-details');

const TRANSF_PATTERN = '/from/@:draweeAccountToken/to/@:assigneeAccountToken';

module.exports = class InitializeTransferRoutes {
    invoke(commander, inquirer) {

        const askTransferQuestions = askTransferDetails(inquirer);

        // commander.post(`/transfer${REC_BASE}${PERIOD_PATTERN}`,
        //     askMovementQuestions, postRecurrentTransfer);

        commander.post(`/transfer${TRANSF_PATTERN}`,
            askTransferQuestions, postTransfer);

        commander.delete('/transfer/:transferId(\\d+)',
            loadTransferById, deleteTransferById);

        commander.get('/transfer',
            getAllTransfers);
    }
};