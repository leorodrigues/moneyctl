

const InitializePresentation = require('./initialize-presentation');
const InitializeSystemRoutes = require('./initialize-system-routes');
const InitializeIncomeRoutes = require('./initialize-income-routes');
const InitializeExpenseRoutes = require('./initialize-expense-routes');
const InitializeAccountRoutes = require('./initialize-account-routes');
const InitializeBalanceRoutes = require('./initialize-balance-routes');
const InitializeBindingRoutes = require('./initialize-binding-routes');
const InitializeTransferRoutes = require('./initialize-transfer-routes');
const InitializeRecurrencyRoutes = require('./initialize-recurrency-routes');
const InitializeGlobalHandlers = require('./initialize-global-handlers');
const InitializeContext = require('./initialize-context');

const presentationConfig = require('./presentation.json');

const initializePresentation = new InitializePresentation();
const initializeContext = new InitializeContext();

const routeInitializations = [
    new InitializeSystemRoutes(),
    new InitializeIncomeRoutes(),
    new InitializeExpenseRoutes(),
    new InitializeAccountRoutes(),
    new InitializeBalanceRoutes(),
    new InitializeBindingRoutes(),
    new InitializeTransferRoutes(),
    new InitializeRecurrencyRoutes(),
    new InitializeGlobalHandlers()
];

module.exports = class Boot {
    run(commander, inquirer) {
        const presentationStreamer =
            initializePresentation.invoke(presentationConfig);

        initializeContext.invoke(commander);

        routeInitializations.forEach(i =>
            i.invoke(commander, inquirer, presentationStreamer));
    }
};
