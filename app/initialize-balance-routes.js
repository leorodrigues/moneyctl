const reportBalance =
    require('../routes/presentation/report-balance');

const computeBalance =
    require('../routes/contexting/compute-balance');

const PERIOD_SEGMENT = '/:year(\\d{4})/until/:endMonth(\\d{2})';

module.exports = class InitializeAccountRoutes {
    invoke(commander) {

        commander.get(`/balance${PERIOD_SEGMENT}`,
            computeBalance, reportBalance);

        commander.get('/balance',
            computeBalance, reportBalance);
    }
};