
const getAllRecurrencies =
    require('../routes/retrieval/get-all-recurrencies');

const deleteRecurrencyById =
    require('../routes/removal/delete-recurrency-by-id');

const loadRecurrencyById =
    require('../routes/contexting/load-recurrency-by-id');

module.exports = class InitializeRecurrencyRoutes {
    invoke(commander) {
        commander.delete('/recurrency/:recurrencyId(\\d+)',
            loadRecurrencyById, deleteRecurrencyById);

        commander.get('/recurrency/:recurrencyId(\\d+)',
            loadRecurrencyById);

        commander.get('/recurrency',
            getAllRecurrencies);
    }
};