
const getAllExpenseLines =
    require('../routes/retrieval/get-all-expense-lines');

const getAllExpenses =
    require('../routes/retrieval/get-all-expenses');

const deleteMovementLineInstance =
    require('../routes/removal/delete-movement-line-instance');

const deleteMovementById =
    require('../routes/removal/delete-movement-by-id');

const postMovement =
    require('../routes/saving/post-movement');

const putMovement =
    require('../routes/saving/put-movement');

const postRecurrentMovement =
    require('../routes/saving/post-recurrent-movement');

const postMovementLine =
    require('../routes/saving/post-movement-line');

const loadMovementLineById =
    require('../routes/contexting/load-movement-line-by-id');

const loadMovementById =
    require('../routes/contexting/load-movement-by-id');

const askMovementLineDetails =
    require('../routes/inquiries/ask-movement-line-details');

const askMovementDetails =
    require('../routes/inquiries/ask-movement-details');

const REC_BASE = '/:times(\\d+)-times';

const PERIOD_PATTERN = '/every-:length(\\d+)-:period(weeks|months)';

module.exports = class InitializeExpenseRoutes {
    invoke(commander, inquirer) {

        const askMovementLineQuestions = askMovementLineDetails(inquirer);

        const askMovementQuestions = askMovementDetails(inquirer);

        commander.delete('/:type(expense)/line/:lineId(\\d+)',
            loadMovementLineById, deleteMovementLineInstance);

        commander.post(`/:type(expense)/line${PERIOD_PATTERN}`,
            askMovementLineQuestions, postMovementLine);

        commander.get('/:type(expense)/line/:lineId(\\d+)',
            loadMovementLineById);

        commander.get('/:type(expense)/line', getAllExpenseLines);

        commander.post(`/:type(expense)${REC_BASE}${PERIOD_PATTERN}`,
            askMovementQuestions, postRecurrentMovement);

        commander.delete('/:type(expense)/:movementId(\\d+)',
            deleteMovementById);

        commander.put('/:type(expense)/:movementId(\\d+)',
            loadMovementById, askMovementQuestions, putMovement);

        commander.get('/:type(expense)/:movementId(\\d+)',
            loadMovementById);

        commander.post('/:type(expense)',
            askMovementQuestions, postMovement);

        commander.get('/:type(expense)', getAllExpenses);
    }
};
