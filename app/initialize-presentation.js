const pad = require('text-padding');

const { PresentationKit, Styles } = require('../engines/presentation');

const { compressTags } = require('../engines/model');

const STYLES = Styles.byName();

const TRANSFORMATIONS = {
    'style': {
        defaultHead: STYLES.fgBlack.bgBlue.value,
        income: STYLES.fgLightCyan.value,
        expense: STYLES.fgLightRed.value
    },

    'transform': {
        upperCase: o => (o.column || o.value || '')
            .toString().toLocaleUpperCase(),

        flattenTags: ({ value }) => compressTags(value),
        isoDate: ({ value }) => value.format('YYYY-MM-DD'),
        twoDecimals: f => f.isEmpty || !f.value ? '-' : f.value.toFixed(2),
        paddedId: ({ value }) => pad(value.toString(), 6, 'left', '0'),
        explainPeriodLength: l => `every ${pad(l.value, 2, 'left', '0')}`,
        explainTime: l => `${pad(l.value, 3, 'left', '0')} times`
    }
};

module.exports = class InitializePresentation {
    invoke(presentationConfig) {
        const presentationKit = new PresentationKit();

        const fragmenter = presentationKit.makeFragmenter();

        const inspector = presentationKit.makeConfigInspector(TRANSFORMATIONS);

        const horizontalStreamer = presentationKit
            .makeHorizontalStreamer(fragmenter, inspector, presentationConfig);

        const verticalStreamer = presentationKit
            .makeVerticalStreamer(fragmenter, inspector, presentationConfig);

        return presentationKit
            .makeOrientedStreamer(50, horizontalStreamer, verticalStreamer);
    }
};