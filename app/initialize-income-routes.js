const getAllIncomeLines =
    require('../routes/retrieval/get-all-income-lines');

const getAllIncomes =
    require('../routes/retrieval/get-all-incomes');

const deleteMovementLineInstance =
    require('../routes/removal/delete-movement-line-instance');

const deleteMovementById =
    require('../routes/removal/delete-movement-by-id');

const postMovement =
    require('../routes/saving/post-movement');

const putMovement =
    require('../routes/saving/put-movement');

const postRecurrentMovement =
    require('../routes/saving/post-recurrent-movement');

const postMovementLine =
    require('../routes/saving/post-movement-line');

const loadMovementLineById =
    require('../routes/contexting/load-movement-line-by-id');

const loadMovementById =
    require('../routes/contexting/load-movement-by-id');

const askMovementLineDetails =
    require('../routes/inquiries/ask-movement-line-details');

const askMovementDetails =
    require('../routes/inquiries/ask-movement-details');

const REC_BASE = '/:times(\\d+)-times';

const PERIOD_PATTERN = '/every-:length(\\d+)-:period(weeks|months)';

module.exports = class InitializeIncomeRoutes {
    invoke(commander, inquirer) {

        const askMovementLineQuestions = askMovementLineDetails(inquirer);

        const askMovementQuestions = askMovementDetails(inquirer);

        commander.delete('/:type(income)/line/:lineId(\\d+)',
            loadMovementLineById, deleteMovementLineInstance);

        commander.post(`/:type(income)/line${PERIOD_PATTERN}`,
            askMovementLineQuestions, postMovementLine);

        commander.get('/:type(income)/line/:lineId(\\d+)',
            loadMovementLineById);

        commander.get('/:type(income)/line', getAllIncomeLines);

        commander.post(`/:type(income)${REC_BASE}${PERIOD_PATTERN}`,
            askMovementQuestions, postRecurrentMovement);

        commander.delete('/:type(income)/:movementId(\\d+)',
            deleteMovementById);

        commander.put('/:type(income)/:movementId(\\d+)',
            loadMovementById, askMovementQuestions, putMovement);

        commander.get('/:type(income)/:movementId(\\d+)',
            loadMovementById);

        commander.post('/:type(income)',
            askMovementQuestions, postMovement);

        commander.get('/:type(income)', getAllIncomes);
    }
};
