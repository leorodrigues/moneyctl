const log = require('../../engines/logging');
const { Account } = require('../../engines/model');

const { TYPE_CREDIT, TYPE_CURRENT } = Account;

const ORIGINAL_QUESTIONS = [
    {
        type: 'input',
        name: 'name',
        message: 'Account name'
    },
    {
        type: 'list',
        name: 'type',
        message: 'Account type',
        default: TYPE_CURRENT,
        choices: [
            { value: TYPE_CURRENT, name: 'Current' },
            { value: TYPE_CREDIT, name: 'Credit' }
        ]
    },
    {
        type: 'confirm',
        name: 'isExtraPeriod',
        default: true,
        message: 'Does it close in the next period?',
        when: a => a.type === TYPE_CREDIT
    },
    {
        type: 'list',
        name: 'billingPeriod',
        default: 'month',
        message: 'What is the billing period',
        choices: [
            { value: 'month', name: 'Months' },
            { value: 'week', name: 'Weeks' }
        ],
        when: a => a.type === TYPE_CREDIT
    },
    {
        type: 'number',
        name: 'billingPeriodLength',
        default: 1,
        message: a => `How many ${a.billingPeriod}s ahead?`,
        when: a => a.type === TYPE_CREDIT
    },
    {
        type: 'number',
        name: 'billingClosureDay',
        default: 1,
        message: 'When does the account closes? (day of the month)',
        when: a => a.type === TYPE_CREDIT && a.billingPeriod === 'month'
    },
    {
        type: 'number',
        name: 'billingDeadlineDistance',
        default: 7,
        message: 'How many consecutive days until the deadline date?',
        when: a => a.type === TYPE_CREDIT && a.billingPeriod === 'month'
    },
    {
        type: 'number',
        name: 'openingBalance',
        message: 'Opening balance',
        default: 0
    }
];

function getPristineQuestions(originalQuestions) {
    return originalQuestions.map(q => Object.assign({}, q));
}

function getQuestionByName(questions, name) {
    return questions.find(q => q.name === name);
}

function assignDefaults(questions) {
    return (fieldName, value) =>
        getQuestionByName(questions, fieldName).default = value;
}

function printTitleIfAvailable(title) {
    if (title) log.print(`${title}\n`);
}

module.exports = function askAccountDetails(inquirerPrompt, optionals = { }) {
    const { title } = optionals;
    return async function gatherAccountDetails(context, next) {
        printTitleIfAvailable(title);
        const { account } = context;
        const questions = getPristineQuestions(ORIGINAL_QUESTIONS);
        if (account)
            account.provideQuestionsDefaults(assignDefaults(questions));
        const answers = await inquirerPrompt(questions);
        context.answers = answers;
        next();
    };
};