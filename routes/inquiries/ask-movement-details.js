const { compressTags } = require('../../engines/model');

const ORIGINAL_QUESTIONS = [
    {
        type: 'datetime',
        name: 'operationDate',
        message: 'Event date',
        format: ['yyyy', '-', 'mm', '-', 'dd']
    },
    {
        type: 'list',
        name: 'account',
        message: 'Account',
    },
    {
        type: 'number',
        name: 'amount',
        message: 'Amount'
    },
    {
        type: 'list',
        name: 'currency',
        default: 'BRL',
        choices: ['BRL', 'USD', 'EUR'],
        message: 'Currency'
    },
    {
        type: 'input',
        name: 'tags',
        message: 'Tags'
    }
];

function getPristineQuestions(originalQuestions) {
    return originalQuestions.map(q => Object.assign({}, q));
}

function getQuestionByName(questions, name) {
    return questions.find(q => q.name === name);
}

function assignDefault(questions, name, value, fieldName = 'default') {
    getQuestionByName(questions, name)[fieldName] = value;
}

function assignDefaults(questions, availableAccounts) {
    return (operationDate, accountToken, amount, currency, tags) => {
        const account = availableAccounts.find(a => a.token === accountToken);
        operationDate = operationDate.toDate();
        assignDefault(questions, 'operationDate', operationDate, 'initial');
        assignDefault(questions, 'account', account);
        assignDefault(questions, 'amount', amount);
        assignDefault(questions, 'currency', currency);
        assignDefault(questions, 'tags', compressTags(tags));
    };
}

function assignAccountChoices(questions, accounts) {
    const question = getQuestionByName(questions, 'account');
    question.choices = accounts.map(a => ({ name: a.name, value: a }));
}

module.exports = function askMovementDetails(inquirerPrompt) {
    return async function gatherMovementDetails(context, next) {
        const { atm, currentMovement } = context;
        const availableAccounts = await atm.findAllAccounts();
        const questions = getPristineQuestions(ORIGINAL_QUESTIONS);
        assignAccountChoices(questions, availableAccounts);
        if (currentMovement)
            currentMovement.provideQuestionDefaults(
                assignDefaults(questions, availableAccounts));
        context.answers = await inquirerPrompt(questions);
        next();
    };
};