const { compressTags } = require('../../engines/model');

const LETTERS = 'abcdefghijklmnopqrstuvxyz'.split('');

function getALetter() {
    const result = LETTERS.shift();
    LETTERS.push(result);
    return result;
}

const ORIGINAL_QUESTIONS = [
    {
        type: 'input',
        name: 'name',
        message: 'Line name',
        default: () => `line-${getALetter()}`
    },
    {
        type: 'list',
        name: 'account',
        message: 'Account',
    },
    {
        type: 'number',
        name: 'amount',
        message: 'Amount'
    },
    {
        type: 'list',
        name: 'currency',
        default: 'BRL',
        choices: ['BRL', 'USD', 'EUR'],
        message: 'Currency'
    },
    {
        type: 'input',
        name: 'tags',
        message: 'Tags'
    },
    {
        type: 'datetime',
        name: 'startDate',
        format: ['yyyy', '-', 'mm', '-', 'dd'],
        message: 'Start date'
    }
];

function getPristineQuestions(originalQuestions) {
    return originalQuestions.map(q => Object.assign({}, q));
}

function getQuestionByName(questions, name) {
    return questions.find(q => q.name === name);
}

function assignDefault(questions, name, value, fieldName = 'default') {
    getQuestionByName(questions, name)[fieldName] = value;
}

function assignDefaults(questions, availableAccounts) {
    return (accountToken, amount, currency, tags, startDate) => {
        const account = availableAccounts.find(a => a.token === accountToken);
        if (startDate) {
            startDate = startDate.toDate();
            assignDefault(questions, 'startDate', startDate, 'initial');
        }
        assignDefault(questions, 'account', account);
        assignDefault(questions, 'amount', amount);
        assignDefault(questions, 'currency', currency);
        assignDefault(questions, 'tags', compressTags(tags));
    };
}

function assignAccountChoices(questions, accounts) {
    const question = getQuestionByName(questions, 'account');
    question.choices = accounts.map(a => ({ name: a.name, value: a }));
}

module.exports = function askLineDetails(inquirerPrompt) {
    return async function gatherLineDetails(context, next) {
        const { atm, incomeLine, expenseLine } = context;
        const availableAccounts = await atm.findAllAccounts();
        const movementLine = incomeLine || expenseLine;
        const questions = getPristineQuestions(ORIGINAL_QUESTIONS);
        assignAccountChoices(questions, availableAccounts);
        if (movementLine)
            movementLine.provideQuestionDefaults(
                assignDefaults(questions, availableAccounts));
        context.answers = await inquirerPrompt(questions);
        next();
    };
};