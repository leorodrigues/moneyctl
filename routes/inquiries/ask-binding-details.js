const LETTERS = 'abcdefghijklmnopqrstuvxyz'.split('');

function getALetter() {
    const result = LETTERS.shift();
    LETTERS.push(result);
    return result;
}

const ORIGINAL_QUESTIONS = [
    {
        type: 'input',
        name: 'name',
        message: 'Line name',
        default: () => `binding-${getALetter()}`
    },
    {
        type: 'input',
        name: 'tags',
        message: 'Tags'
    }
];

function getPristineQuestions(originalQuestions) {
    return originalQuestions.map(q => Object.assign({}, q));
}

function getQuestionByName(questions, name) {
    return questions.find(q => q.name === name);
}

function assignDefault(questions, name, value, fieldName = 'default') {
    getQuestionByName(questions, name)[fieldName] = value;
}

function skipPredicates(name) {
    return name !== 'predicates';
}

function serializeTags(tagMap) {
    const keys = Object.keys(tagMap);
    const result = keys.filter(skipPredicates).map(k => `${k}:${tagMap[k]}`);
    result.push(...tagMap.predicates);
    return result.join(';');
}

function assignDefaults(questions, binding) {
    assignDefault(questions, 'name', serializeTags(binding.name));
    assignDefault(questions, 'tags', serializeTags(binding.tags));
}

module.exports = function askBindingDetails(inquirerPrompt) {
    return async function gatherBindingDetails(context, next) {
        const { currentBinding } = context;
        const questions = getPristineQuestions(ORIGINAL_QUESTIONS);
        if (currentBinding) assignDefaults(questions, currentBinding);
        context.answers = await inquirerPrompt(questions);
        next();
    };
};