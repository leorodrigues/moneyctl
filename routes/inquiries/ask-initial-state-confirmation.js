const moment = require('moment');
const sha1 = require('sha1');
const { cyan, yellow } = require('chalk');
const log = require('../../engines/logging');
const { reportError } = require('../../engines/reporting');

function makeQuestions(password) {
    return [
        {
            type: 'confirm',
            name: 'initialize',
            message: 'Confirm database initialization? All data will be lost!',
            default: false
        },
        {
            type: 'input',
            name: 'password',
            message: `Type the sequence ${cyan(password)} to confirm:`,
            when: a => a.initialize
        }
    ];
}

function makeInvalidPasswordMsg(password) {
    return `Sequence ${yellow(password)} is not the required one.`;
}

module.exports = function askInitialStateConfirmation(inquirerPrompt) {
    return async function gatherAccountDetails(context, next) {
        const password = sha1(moment().toISOString())
            .toUpperCase()
            .substring(0, 8);
        const answers = await inquirerPrompt(makeQuestions(password));
        if (!answers.initialize) return;
        if (answers.password !== password) {
            log.print(reportError(makeInvalidPasswordMsg(answers.password)));
            return;
        }
        next();
    };
};