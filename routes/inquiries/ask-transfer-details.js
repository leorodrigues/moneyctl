
const ORIGINAL_QUESTIONS = [
    {
        type: 'input',
        name: 'name',
        message: 'Transfer name'
    },
    {
        type: 'datetime',
        name: 'operationDate',
        message: 'Event date',
        format: ['yyyy', '-', 'mm', '-', 'dd']
    },
    {
        type: 'number',
        name: 'amount',
        message: 'Amount'
    },
    {
        type: 'list',
        name: 'currency',
        default: 'BRL',
        choices: ['BRL', 'USD', 'EUR'],
        message: 'Currency'
    },
    {
        type: 'input',
        name: 'tags',
        message: 'Tags'
    }
];

function getPristineQuestions(originalQuestions) {
    return originalQuestions.map(q => Object.assign({}, q));
}

module.exports = function askTransferDetails(inquirerPrompt) {
    return async function gatherTransferDetails(context, next) {
        const questions = getPristineQuestions(ORIGINAL_QUESTIONS);
        context.answers = await inquirerPrompt(questions);
        next();
    };
};