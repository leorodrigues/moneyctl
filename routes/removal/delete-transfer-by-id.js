const log = require('../../engines/logging');

module.exports = async function deleteTransferById(context) {
    const { atm, transfer } = context;
    await atm.deleteTransferInstance(transfer);
    log.print('Done.');
};
