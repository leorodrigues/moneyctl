const log = require('../../engines/logging');

module.exports = async function deleteAccountById(context) {
    const { atm, params: { accountId } } = context;
    await atm.deleteAccountById(accountId);
    log.print('Done.');
};
