const log = require('../../engines/logging');

module.exports = async function deleteMovementLineInstance(context) {
    const { atm, incomeLine, expenseLine } = context;
    await atm.deleteMovementLineInstance(incomeLine || expenseLine);
    log.print('Done.');
};
