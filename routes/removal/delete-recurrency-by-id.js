const log = require('../../engines/logging');

module.exports = async function deleteRecurrencyById(context) {
    const { atm, recurrency } = context;
    await atm.deleteRecurrencyInstance(recurrency);
    log.print('Done.');
};
