const log = require('../../engines/logging');

module.exports = async function deleteMovementById(context) {
    const { atm, params: { movementId } } = context;
    await atm.deleteMovementById(movementId);
    log.print('Done.');
};
