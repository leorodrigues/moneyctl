const log = require('../../engines/logging');

module.exports = async function deleteBindingById(context) {
    const { atm, params: { bindingId } } = context;
    await atm.deleteBindingById(bindingId);
    log.print('Done');
};
