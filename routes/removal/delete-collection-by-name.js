const log = require('../../engines/logging');
const db = require('../../db/support');
const { reportError } = require('../../engines/reporting');

module.exports = async function postDropCollection(context) {
    const { params: { collectionName } } = context;

    try {
        await db.dropCollectionByName(collectionName);
        log.print('Done.');
    } catch(error) {
        log.print(reportError(error.message));
    }
};