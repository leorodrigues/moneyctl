module.exports = async function getAllRecurrencies(context, next) {
    const { atm } = context;
    const stream = await atm.streamAllRecurrencies();
    context.streams.push({ stream, name: 'Recurrency' });
    next();
};