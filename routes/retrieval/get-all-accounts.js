module.exports = async function getAllAccounts(context, next) {
    const { atm } = context;
    const stream = await atm.streamAllAccounts();
    context.streams.push({ stream, name: 'Account' });
    next();
};