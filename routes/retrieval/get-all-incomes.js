module.exports = async function getAllIncomes(context, next) {
    const { atm } = context;
    const stream = await atm.streamAllIncomes();
    context.streams.push({ stream, name: 'Income' });
    next();
};