module.exports = async function getAllExpenseLines(context, next) {
    const { atm } = context;
    const stream = await atm.streamAllExpenseLines();
    context.streams.push({ stream, name: 'ExpenseLine' });
    next();
};