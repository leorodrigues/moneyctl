module.exports = async function getAllTransfers(context, next) {
    const { atm } = context;
    const stream = await atm.streamAllTransfers();
    context.streams.push({ stream, name: 'Transfer' });
    next();
};