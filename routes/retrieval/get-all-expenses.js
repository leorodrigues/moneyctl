module.exports = async function getAllExpenses(context, next) {
    const { atm } = context;
    const stream = await atm.streamAllExpenses();
    context.streams.push({ stream, name: 'Expense' });
    next();
};