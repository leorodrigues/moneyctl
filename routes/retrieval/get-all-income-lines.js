module.exports = async function getAllIncomeLines(context, next) {
    const { atm } = context;
    const stream = await atm.streamAllIncomeLines();
    context.streams.push({ stream, name: 'IncomeLine' });
    next();
};