module.exports = async function getAllBindings(context, next) {
    const { atm } = context;
    const stream = await atm.streamAllBindings();
    context.streams.push({ stream, name: 'Binding' });
    next();
};