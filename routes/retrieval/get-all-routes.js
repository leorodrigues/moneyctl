module.exports = async function getAllRoutes(context, next) {
    const { commander } = context;
    context.streams.push({ stream: commander.streamHelp(), name: 'Help' });
    next();
};