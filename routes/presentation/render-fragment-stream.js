
const { FragmentRenderer } = require('../../engines/presentation');

const log = require('../../engines/logging');

const REDERER = new FragmentRenderer();

module.exports = async function renderFragmentStream(context, next) {
    const { fragmentStream } = context;
    if (!fragmentStream) return next();
    await log.asyncPipeAndConsumeStream(fragmentStream.pipe(REDERER.render()));
    next();
};
