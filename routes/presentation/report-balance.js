const moment = require('moment');

const { FragmentStyler, FragmentParser, FragmentShaper } =
    require('../../engines/presentation');

const BalanceStreamer = require('./balance-streamer');

const STREAMER = new BalanceStreamer();

const twoDecimals = c =>c.isEmpty || !c.value ? '-' : c.value.toFixed(2);

function months(startDate, endDate) {
    return Math.round(moment.duration(endDate.diff(startDate)).asMonths());
}

function willFitScreen(interval) {
    return process.stdout.columns > interval * 10 + 22;
}

function configureHorizontalReport(balance) {
    return STREAMER.streamHorizontaly(balance);
}

function configureVerticalReport(balance) {
    return STREAMER.streamVerticaly(balance);
}

module.exports = async function reportBalance(context, next) {
    const { currentBalance: balance, startDate, endDate } = context;
    
    const parser = new FragmentParser();
    const shaper = new FragmentShaper();
    const styler = new FragmentStyler();

    parser.fragment('heading', ({ value }) => value.toLocaleUpperCase());
    parser.fragment('date', ({ value }) => value.format('YYYY-MM'));
    parser.fragment('dateHeading', ({ value }) => value.format('YYYY-MM'));
    parser.fragment('cell', c => twoDecimals(c));

    shaper.shape('cell', 9, 1, 'right');
    shaper.shape('date', 9);
    shaper.shape('label', 12);
    shaper.shape('heading', 21);
    shaper.shape('dateHeading', 21);

    const monthsInterval = months(startDate, endDate);

    const stream = willFitScreen(monthsInterval)
        ? configureHorizontalReport(balance, styler)
        : configureVerticalReport(balance, styler);

    context.fragmentStream = stream
        .pipe(parser.parse())
        .pipe(styler.stylize())
        .pipe(shaper.output());

    next();
};
