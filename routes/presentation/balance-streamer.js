const { Readable } = require('stream');
const { NEW_LINE } = require('../../engines/presentation');

module.exports = class BalanceStreamer {
    streamHorizontaly(balance) {
        const dates = balance.iterateDateFragments();
        const accounts = balance.iterateAccountFragments();
        return new Readable({
            objectMode: true,
            read() {
                for (const fragment of horizontal(dates, accounts, balance))
                    this.push(fragment);
                this.push(null);
            }
        });
    }

    streamVerticaly(balance) {
        const dates = balance.iterateDateFragments();
        const accounts = balance.iterateAccountFragments();
        return new Readable({
            objectMode: true,
            read() {
                for (const fragment of vertical(dates, accounts, balance))
                    this.push(fragment);
                this.push(null);
            }
        });
    }
};

function *horizontal(dates, accounts, balance) {
    dates = [...dates];
    const outstandingElements = [ ];
    for (const account of accounts) {
        while (outstandingElements.length)
            yield outstandingElements.shift();

        const summary = { };
        const credits = { };
        const debits = { };
        for (let i = 0; i < dates.length; i++) {
            const date = dates[i];

            let fragments = balance.iterateSummaryFragments(date, account);
            collect(summary, fragments, i);

            fragments = balance.iterateCreditLineFragments(date, account);
            collect(credits, fragments, i);

            fragments = balance.iterateDebitLineFragments(date, account);
            collect(debits, fragments, i);
        }
        Object.values(summary).forEach(a => fillWithNulls(a, dates.length));
        Object.values(credits).forEach(a => fillWithNulls(a, dates.length));
        Object.values(debits).forEach(a => fillWithNulls(a, dates.length));

        yield reFrag(account, 'heading');
        yield NEW_LINE;
        yield { frag: 'heading', value: 'Summary' };
        yield NEW_LINE;
        yield *traverse(summary, dates);
        yield NEW_LINE;
        yield { frag: 'heading', value: 'Credits' };
        yield NEW_LINE;
        yield *traverse(credits, dates);
        yield NEW_LINE;
        yield { frag: 'heading', value: 'Debits' };
        yield NEW_LINE;
        yield *traverse(debits, dates);

        outstandingElements.push(NEW_LINE);
    }
}

function *vertical(dates, accounts, balance) {
    dates = [...dates];
    for (const account of accounts) {
        yield reFrag(account, 'heading');
        yield NEW_LINE;

        const outstandingElements = [ ];
        for (let i = 0; i < dates.length; i++) {
            while (outstandingElements.length)
                yield outstandingElements.shift();

            const date = dates[i];
            yield reFrag(date, 'dateHeading');
            yield NEW_LINE;

            const summary = balance.iterateSummaryFragments(date, account);
            yield *traverseSplit(
                summary, { frag: 'heading', value: 'Summary' }, NEW_LINE);

            const credits = balance.iterateCreditLineFragments(date, account);
            yield *traverseSplit(
                credits, { frag: 'heading', value: 'Credits' }, NEW_LINE);

            const debits = balance.iterateDebitLineFragments(date, account);
            yield *traverseSplit(
                debits, { frag: 'heading', value: 'Debits' }, NEW_LINE);

            outstandingElements.push(NEW_LINE);
        }
    }
}

function *traverseSplit(fragments, ...outstandingElements) {
    for (const fragment of fragments) {
        while (outstandingElements.length)
            yield outstandingElements.shift();
        const { name } = fragment;
        yield { frag: 'label', value: name };
        yield reFrag(fragment, 'cell', 'dataPoint');
        yield NEW_LINE;
    }
}

function *traverse(lineup, dates) {
    yield *many({ frag: 'label', value: 'Items' }, dates);
    yield NEW_LINE;
    for (const name in lineup) {
        const fragments = lineup[name];
        yield { frag: 'label', value: name };
        for (const f of fragments)
            yield fragOrEmpty(f, name);
        yield NEW_LINE;
    }
}

function fragOrEmpty(fragment, name) {
    return fragment ? reFrag(fragment, 'cell', 'dataPoint') : {
        frag: 'cell', name, isEmpty: true
    };
}

function reFrag(fragment, newFrag, oldFrag) {
    if (oldFrag && fragment.frag !== oldFrag) return fragment;
    fragment.frag = newFrag;
    return fragment;
}

function collect(lineup, fragments, level) {
    for (const fragment of fragments) {
        const { name } = fragment;
        const existing = safeGet(lineup, name, [ ]);
        fillWithNulls(existing, level);
        existing.push(fragment);
    }
}

function fillWithNulls(array, finalLength) {
    const actualLength = array.length;
    if (actualLength < finalLength)
        for (let i = 0; i < finalLength - actualLength; i++)
            array.push(null);
}

function safeGet(map, key, def) {
    if (key in map) return map[key];
    map[key] = def;
    return def;
}

function *many(head, tail) {
    yield head;
    for (const t of tail)
        yield t;
}
