const StreamConcat = require('stream-concat');

module.exports = function handleFragmentDataForDisplay(service) {
    return async function fragmentDataForDisplay(context, next) {
        const { fragmentStream } = context;
        const pending = [];

        if (fragmentStream)
            pending.push(() => fragmentStream);

        appendStreams(context, pending, service);

        appendBindings(context, pending, service);

        appendAccounts(context, pending, service);

        appendMovementLines(context, pending, service);

        appendRecurrency(context, pending, service);

        appendTransfer(context, pending, service);

        appendMovements(context, pending, service);

        context.fragmentStream = new StreamConcat(
            nextStream(pending), { objectMode: true });

        next();
    };
};

function appendStreams(context, pending, service) {
    const { streams } = context;
    for (const { stream, name } of streams)
        pending.push(() => service.presentTable(stream, name));
}

function appendBindings(context, pending, service) {
    const { bindings } = context;
    
    if (bindings)
        pending.push(() => service.presentTable(
            service.streamInstances(...bindings), 'Binding'));
}

function appendMovementLines(context, pending, service) {
    const { incomeLine, expenseLine } = context;

    if (incomeLine)
        pending.push(() => service.presentTable(
            service.streamInstances(incomeLine), 'IncomeLine'));

    if (expenseLine)
        pending.push(() => service.presentTable(
            service.streamInstances(expenseLine), 'ExpenseLine'));
}

function appendRecurrency(context, pending, service) {
    const { recurrency } = context;

    if (recurrency)
        pending.push(() => service.presentTable(
            service.streamInstances(recurrency), 'Recurrency'));
}

function appendTransfer(context, pending, service) {
    const { transfer } = context;

    if (transfer)
        pending.push(() => service.presentTable(
            service.streamInstances(transfer), 'Transfer'));
}

function appendMovements(context, pending, service) {
    const { incomes, expenses } = context;

    if (incomes)
        pending.push(() => service.presentTable(
            service.streamInstances(...incomes), 'Income'));

    if (expenses)
        pending.push(() => service.presentTable(
            service.streamInstances(...expenses), 'Expense'));
}

function appendAccounts(context, pending, service) {
    const { accounts } = context;

    if (accounts)
        pending.push(() => service.presentTable(
            service.streamInstances(...accounts), 'Account'));
}

function nextStream(pending) {
    return () => pending.length ? pending.shift()() : null;
}