const log = require('../../engines/logging');
const { reportMovementIdNotFound } = require('../../engines/reporting');

module.exports = async function loadMovementById(context, next) {
    const { atm, type, params: { movementId } } = context;
    const result = await atm.findMovementById(movementId);
    if (result) {
        bindMovementToContext(result, context, type);
        next();
        return;
    }
    log.print(reportMovementIdNotFound(movementId));
};

function bindMovementToContext(result, context, type) {
    if (type === 'expense')
        context.expenses = [result];
    else
        context.incomes = [result];
}