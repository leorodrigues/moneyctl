const log = require('../../engines/logging');
const { reportAccountIdNotFound } = require('../../engines/reporting');

module.exports = async function loadBindingAccountsByToken(context, next) {
    const { atm } = context;
    const { params: { draweeAccountToken, assigneeAccountToken } } = context;
    const draweeAccount = await atm.findAccountByToken(draweeAccountToken);
    const assigneeAccount = await atm.findAccountByToken(assigneeAccountToken);

    if (!draweeAccount) {
        log.print(reportAccountIdNotFound(draweeAccountToken));
        return;
    }

    if (!assigneeAccount) {
        log.print(reportAccountIdNotFound(assigneeAccountToken));
        return;
    }

    context.assigneeAccount = assigneeAccount;
    context.draweeAccount = draweeAccount;

    next();
};
