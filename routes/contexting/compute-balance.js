const moment = require('moment');
const log = require('../../engines/logging');

function makeErrorMessage(year, end, error) {
    const datePart = `from ${ year }/1 to ${ year }/${ end }`;
    return `Unable to compute balance - ${ datePart } - ${ error }`;
}

module.exports = async function computeBalance(context, next) {
    const today = moment().startOf('date');
    const { atm, params } = context;
    const { year = today.year(), endMonth = today.format('MM') } = params;
    try {
        const startDate = moment(`${ year }-01-01`).startOf('date');
        const endDate = moment(`${ year }-${ endMonth }-01`).endOf('month');
        context.currentBalance = await atm.computeBalance(startDate, endDate);
        context.startDate = startDate;
        context.endDate = endDate;
        next();
    } catch(error) {
        log.error(makeErrorMessage(year, endMonth, error.message));
    }
};
