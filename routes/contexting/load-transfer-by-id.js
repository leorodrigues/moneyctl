const log = require('../../engines/logging');
const { reportTransferIdNotFound } = require('../../engines/reporting');


module.exports = async function loadRecurrencyById(context, next) {
    const { atm, params: { transferId } } = context;
    const result = await atm.findTransferById(transferId);
    if (result) {
        context.transfer = result;
        next();
        return;
    }
    log.print(reportTransferIdNotFound(transferId));
};
