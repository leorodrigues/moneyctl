const log = require('../../engines/logging');
const { reportBindingIdNotFound } = require('../../engines/reporting');

module.exports = async function loadBindingById(context, next) {
    const { atm, params: { bindingId } } = context;
    const instance = await atm.findBindingById(bindingId);
    if (instance) {
        context.bindings = [instance];
        next();
        return;
    }
    log.print(reportBindingIdNotFound(bindingId));
};
