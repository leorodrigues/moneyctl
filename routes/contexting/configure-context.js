module.exports = function configureContext(atm) {
    return async function handleConfigureContext(context, next) {
        context.streams = [ ];
        context.atm = atm;
        next();
    };
};
