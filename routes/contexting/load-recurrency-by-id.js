const log = require('../../engines/logging');
const { reportRecurrencyIdNotFound } = require('../../engines/reporting');

module.exports = async function loadRecurrencyById(context, next) {
    const { atm, params: { recurrencyId } } = context;
    const result = await atm.findRecurrencyById(recurrencyId);
    if (result) {
        context.recurrency = result;
        next();
        return;
    }
    log.print(reportRecurrencyIdNotFound(recurrencyId));
};
