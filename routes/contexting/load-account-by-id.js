const log = require('../../engines/logging');
const { reportAccountIdNotFound } = require('../../engines/reporting');

module.exports = async function loadAccountById(context, next) {
    const { atm, params: { accountId } } = context;
    const result = await atm.findAccountById(accountId);
    if (result) {
        context.accounts = [result];
        next();
        return;
    }
    log.print(reportAccountIdNotFound(accountId));
};
