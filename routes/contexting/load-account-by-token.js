const log = require('../../engines/logging');
const { reportAccountIdNotFound } = require('../../engines/reporting');

module.exports = async function loadAccountByToken(context, next) {
    const { atm, params: { accountToken } } = context;
    const result = await atm.findAccountByToken(accountToken);
    if (result) {
        context.accounts = [result];
        next();
        return;
    }
    log.print(reportAccountIdNotFound(accountToken));
};
