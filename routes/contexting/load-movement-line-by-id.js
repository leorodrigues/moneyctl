const log = require('../../engines/logging');
const { reportMovementLineIdNotFound } = require('../../engines/reporting');

module.exports = async function loadMovementLineById(context, next) {
    const { params: { lineId, type } } = context;
    if (await handleByType(context, type, lineId)) {
        next();
        return;
    }
    log.print(reportMovementLineIdNotFound(lineId));
};

function handleByType(context, type, lineId) {
    if (type === 'expense')
        return handleExpense(context, lineId);
    return handleIncome(context, lineId);
}

async function handleIncome(context, lineId) {
    const { atm } = context;
    const result = await atm.findIncomeLineById(lineId);
    if (!result) return false;
    context.incomeLine = result;
    return true;
}

async function handleExpense(context, lineId) {
    const { atm } = context;
    const result = await atm.findExpenseLineById(lineId);
    if (!result) return false;
    context.expenseLine = result;
    return true;
}
