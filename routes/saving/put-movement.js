module.exports = async function putMovement(context, next) {
    if (context.type === 'expense')
        await putExpense(context);
    else
        await putIncome(context);
    next();
};

async function putIncome(context) {
    const { answers, currentMovement, atm } = context;
    context.incomes = [await atm.updateIncome(answers, currentMovement)];
}

async function putExpense(context) {
    const { answers, currentMovement, atm } = context;
    context.expenses = [await atm.updateExpense(answers, currentMovement)];
}
