const log = require('../../engines/logging');
const { reportError } = require('../../engines/reporting');
const db = require('../../db/support');

module.exports = async function putInitialState(context, next) {
    try {
        await db.dropCollectionByName('accounts');
        await db.dropCollectionByName('movements');
        await db.dropCollectionByName('movement-lines');
        await db.dropCollectionByName('bindings');
        await db.dropCollectionByName('recurrencies');
        await db.dropCollectionByName('bills');
        await db.dropCollectionByName('closures');
        await db.dropCollectionByName('transfers');
        log.print('Done removing data.');
        next();
    } catch(error) {
        log.print(reportError(error.message));
    }
};