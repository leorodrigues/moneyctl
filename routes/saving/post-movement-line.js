module.exports = async function postMovementLine(context, next) {
    const { params: { type } } = context;

    if (type === 'income')
        handleIncome(context);
    else
        handleExpense(context);

    next();
};

async function handleIncome(context) {
    const { params, answers, atm } = context;
    context.incomeLine = await atm.addIncomeLine(
        prepareData(params, answers));
}

async function handleExpense(context) {
    const { params, answers, atm } = context;
    context.expenseLine = await atm.addExpenseLine(
        prepareData(params, answers));
}

function prepareData(params, answers) {
    const { length, period } = params;
    const { name, account, amount, currency, tags, startDate } = answers;

    return {
        name, account, amount, currency, tags, startDate, length, period
    };
}
