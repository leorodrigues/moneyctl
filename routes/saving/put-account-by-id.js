module.exports = async function putAccountById(context, next) {
    const { answers, account, atm } = context;
    const instance = await atm.updateAccount(account, answers);
    context.accounts = [instance];
    next();
};
