module.exports = async function postBinding(context, next) {
    const { answers, assigneeAccount, draweeAccount, atm } = context;
    const binding = await atm.bindAccounts(
        assigneeAccount, draweeAccount, answers);

    context.bindings = [binding];

    next();
};
