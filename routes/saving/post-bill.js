const log = require('../../engines/logging');

module.exports = async function postBill(context) {
    const { atm, accounts } = context;
    await atm.addBill(accounts[0]);
    log.print('Done.');
};