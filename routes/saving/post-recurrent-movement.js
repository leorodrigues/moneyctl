module.exports = async function postRecurrentMovement(context, next) {
    if (context.type === 'expense')
        postRecurrentExpense(context);
    else
        postRecurrentIncome(context);
    next();
};

async function postRecurrentExpense(context) {
    const { recurrency, expenses } = await context.atm.addRecurrentExpense(
        prepareData(context));

    context.recurrency = recurrency;
    context.expenses = expenses;
}

async function postRecurrentIncome(context) {
    const { recurrency, incomes } = await context.atm.addRecurrentIncome(
        prepareData(context));

    context.recurrency = recurrency;
    context.incomes = incomes;
}

function prepareData(context) {
    const { params, answers } = context;
    const { times, period, length: periodLength } = params;
    const { account, amount, currency, tags } = answers;
    const { operationDate: startDate } = answers;
    return {
        times, period, periodLength, account, amount, currency, tags,
        startDate
    };
}