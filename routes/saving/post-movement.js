module.exports = async function postMovement(context, next) {
    if (context.type === 'expense')
        postExpense(context);
    else
        postIncome(context);
    next();
};

async function postExpense(context) {
    const { answers, atm } = context;
    context.expenses = [await atm.addExpense(answers)];
}

async function postIncome(context) {
    const { answers, atm } = context;
    context.incomes = [await atm.addIncome(answers)];
}
