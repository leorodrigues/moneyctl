module.exports = async function postAccount(context, next) {
    const { answers, atm } = context;

    const instance = await atm.openAccount(answers);
    context.accounts = [instance];

    next();
};
