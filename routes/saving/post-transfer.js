module.exports = async function postTransfer(context, next) {
    const { atm, params, answers } = context;
    const { draweeAccountToken, assigneeAccountToken } = params;
    const draweeAcount = await atm.findByAccountToken(draweeAccountToken);
    const assigneeAccount = await atm.findByAccountToken(assigneeAccountToken);

    const { transfer, income, expense } =
        await atm.makeTransfer(draweeAcount, assigneeAccount, answers);

    context.transfer = transfer;
    context.incomes = [income];
    context.expenses = [expense];

    next();
};
