const moment = require('moment');
const { assistedClonning } = require('../clonning');

const DIRECT_FIELDS = [
    '_id',
    'token',
    'accountToken',
    'isExtraPeriod',
    'period',
    'periodLength',
    'closureDay',
    'deadlineDistance',
    'openingDate',
    'closingDate',
    'closed'
];

class Bill {
    constructor(data = { }) {
        assistedClonning(data, undefined, DIRECT_FIELDS, this);
    }

    get serializable() {
        function momentToDate(momentValue, updateField) {
            updateField(momentValue && momentValue.toDate());
        } 
        function register(e) {
            e.on('openingDate', momentToDate);
            e.on('closingDate', momentToDate);
        }
        return assistedClonning(this, register, DIRECT_FIELDS);
    }

    closedClone() {
        const closedInstance = { closed: true };
        return assistedClonning(this, undefined, DIRECT_FIELDS, closedInstance);
    }

    isClosedAsOf(date) {
        return this.closed || date.isAfter(this.closingDate);
    }

    static desserialize(data) {
        data.openingDate = moment(data.openingDate);
        data.closingDate = moment(data.closingDate);
        return new Bill(data);
    }
}

module.exports = Bill;