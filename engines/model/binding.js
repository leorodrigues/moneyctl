const { assistedClonning } = require('../clonning');

const FIELDS = [
    '_id',
    'token',
    'name',
    'assigneeAccountToken',
    'draweeAccountToken',
    'tags'
];

class Binding {
    constructor(data = { }) {
        assistedClonning(data, undefined, FIELDS, this);
    }

    appendToSequence(bindingSequencer) {
        const { token, name, tags } = this;
        bindingSequencer.onAssigneeAccount(this.assigneeAccountToken)
            .onDraweeAccount(this.draweeAccountToken)
            .saveBinding(token, name, tags);
    }

    *iterateFragments() {
        yield { frag: 'id', value: this._id };
        yield { frag: 'name', value: this.name };
        yield { frag: 'drawee', value: this.draweeAccountToken };
        yield { frag: 'assignee', value: this.assigneeAccountToken };
        yield { frag: 'tags', value: this.tags };
    }

    get serializable() {
        return assistedClonning(this, undefined, FIELDS);
    }

    static desserialize(data) {
        return new Binding(data);
    }
}

module.exports = Binding;