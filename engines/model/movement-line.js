const { assistedClonning } = require('../clonning');
const Activity = require('./activity');

const FIELDS = [
    '_id',
    'token',
    'name',
    'accountToken',
    'currency',
    'type',
    'tags',
    'activity'
];

module.exports = class MovementLine {
    constructor(data = { }) {
        assistedClonning(data, undefined, FIELDS, this);
    }

    appendToBalance(balance) {
        const aggregator = balance.onAccount(this.accountToken);
        balance.underTimeInterval((start, end) => {
            const amountByDate = this.activity.iterateAmountByDate(start, end);
            for (const { date, amount } of amountByDate)
                aggregator.onDate(date)
                    .onMovementType(this.type)
                    .aggregate(amount, this.tags, this.token);
        });
    }

    *iterateFragments() {
        yield { frag: 'id', value: this._id };
        yield { frag: 'name', value: this.name };
        yield { frag: 'account', value: this.accountToken };
        yield { frag: 'tags', value: this.tags };
    }

    get serializable() {
        function register(e) {
            e.on('activity', (a, update) => update(a.serializable));
        }
        return assistedClonning(this, register, FIELDS);
    }

    static desserialize(data) {
        data.activity = Activity.desserialize(data.activity);
        return new MovementLine(data);
    }
};