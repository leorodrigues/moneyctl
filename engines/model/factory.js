const sha1 = require('sha1');
const moment = require('moment');

const { expandTags } = require('./tagging');
const { makeTokenFromName } = require('./support');

const Bill = require('./bill');
const Account = require('./account');
const Binding = require('./binding');
const Closure = require('./closure');
const Activity = require('./activity');
const Transfer = require('./transfer');
const Movement = require('./movement');
const Recurrency = require('./recurrency');
const MovementLine = require('./movement-line');

class Factory {

    makeClosure(data) {
        return new Closure(data);
    }

    makeTransfer(
        { name, tags, amount, operationDate, draweeAccount, assigneeAccount }) {
        operationDate = moment(operationDate);
        tags = expandTags(tags);
        const token = makeTokenFromName(name);
        return draweeAccount.provideTransferData(draweeAccountToken =>
            assigneeAccount.provideTransferData(assigneeAccountToken =>
                new Transfer({
                    token,
                    name,
                    tags,
                    amount,
                    operationDate,
                    draweeAccountToken,
                    assigneeAccountToken
                })));
    }

    makeTransferIncome({ transfer }) {
        function packData(...params) {
            const [
                transferToken,, accountToken, currency, amount,
                tags, operationDate
            ] = params;
            return {
                type: Movement.TYPE_CREDIT,
                transferToken, accountToken, currency, amount,
                tags, operationDate
            };
        }
        return new Movement(transfer.provideMovementData(packData));
    }

    makeTransferExpense({ transfer }) {
        function packData(...params) {
            const [
                transferToken, accountToken,, currency, amount,
                tags, operationDate
            ] = params;
            return {
                type: Movement.TYPE_DEBIT,
                transferToken, accountToken, currency, amount,
                tags, operationDate
            };
        }
        return new Movement(transfer.provideMovementData(packData));
    }

    makeMovementLine({ name, account, amount, currency, type, tags, length,
        period, startDate }) {

        startDate = moment(startDate);
        const activity = new Activity({ startDate, period, length, amount });
        const token = makeTokenFromName(name);
        tags = expandTags(`${tags};`.replace(/;;/g, ';'));

        return account.provideMovementLineData(accountToken => {
            return new MovementLine({
                name,
                accountToken,
                currency,
                tags,
                token,
                type,
                activity
            });
        });
    }

    makeIncome({ operationDate, amount, currency, tags, account }) {
        operationDate = moment(operationDate);
        function packData(accountToken) {
            return {
                type: Movement.TYPE_CREDIT,
                accountToken,
                operationDate,
                amount,
                currency,
                tags: expandTags(tags)
            };
        }
        return new Movement(account.provideMovementData(packData));
    }

    makeExpense({ operationDate, amount, currency, tags, account }) {
        operationDate = moment(operationDate);
        function packData(accountToken) {
            return {
                type: Movement.TYPE_DEBIT,
                accountToken,
                operationDate,
                amount,
                currency,
                tags: expandTags(tags)
            };
        }
        return new Movement(account.provideMovementData(packData));
    }

    makeExpenseFromBill({ operationDate, amount, currency, tags, bill }) {
        operationDate = moment(operationDate);
        function packData(billingDate, accountToken, billToken) {
            return {
                operationDate,
                billingDate,
                amount,
                currency,
                tags,
                accountToken,
                billToken
            };
        }
        return new Movement(bill.provideMovementDetails(packData));
    }

    makeRecurrency({ type, times, period, periodLength }) {
        const token = sha1(moment().toISOString());
        return new Recurrency({ type, times, period, periodLength, token });
    }

    makeBinding({ name, tags, assigneeAccount, draweeAccount }) {
        const token = makeTokenFromName(name);
        return assigneeAccount.provideBindingData(assigneeAccountToken => {
            return draweeAccount.provideBindingData(draweeAccountToken => {
                return new Binding({
                    token, name, tags, assigneeAccountToken, draweeAccountToken
                });
            });
        });
    }

    makeBill({ account, openingDate }) {
        function packData(
            period, periodLength, closureDay, deadlineDistance, isExtraPeriod,
            accountToken, openingDate, billingDate) {
            return {
                period,
                periodLength,
                closureDay,
                deadlineDistance,
                isExtraPeriod,
                token: sha1(moment().toISOString()),
                accountToken,
                openingDate,
                billingDate,
                closed: false,
            };
        }
        return new Bill(account.provideBillData(openingDate, packData));
    }

    makeCurrentAccount({ name, openingBalance = 0 }) {
        const token = makeTokenFromName(name);
        return new Account({
            name, type: Account.TYPE_CURRENT, openingBalance, token
        });
    }

    makeCreditAccount({ name, openingBalance = 0, isExtraPeriod,
        billingPeriod, billingPeriodLength, billingClosureDay,
        billingDeadlineDistance }) {
        const token = makeTokenFromName(name);

        const billing = {
            isExtraPeriod: isExtraPeriod,
            period: billingPeriod,
            periodLength: billingPeriodLength,
        };
        if (Account.isMonthly(billingPeriod)) {
            billing.closureDay = billingClosureDay;
            billing.deadlineDistance = billingDeadlineDistance;
        }

        return new Account({
            name, type: Account.TYPE_CREDIT, openingBalance, billing, token
        });
    }

    makeUpdatingAccountCopy(newData, subjectAccount) {
        const instance = Account.isCredit(newData.type)
            ? this.makeCreditAccount(newData)
            : this.makeCurrentAccount(newData);
        instance.impersonate(subjectAccount);
        return instance;
    }

    makeUpdatingIncomeCopy(newData, subjectIncome) {
        const instance = this.makeIncome(newData);
        instance.impersonate(subjectIncome);
        return instance;
    }

    makeUpdatingExpenseCopy(newData, subjectExpense) {
        const instance = this.makeExpense(newData);
        instance.impersonate(subjectExpense);
        return instance;
    }

    generateExpensesFromRecurrency(
        { startDate, amount, currency, tags, account, recurrency }) {

        function makeGenerator(recurrencyToken, dates) {
            function *generateExpenses(accountToken) {
                for (const operationDate of dates) {
                    const data = {
                        type: Movement.TYPE_DEBIT,
                        accountToken,
                        recurrencyToken,
                        operationDate,
                        amount,
                        currency,
                        tags: expandTags(tags)
                    };
                    yield new Movement(data);
                }
            }
            return account.provideMovementData(generateExpenses);
        }

        return recurrency.provideMovementData(startDate, makeGenerator);
    }

    generateIncomesFromRecurrency(
        { startDate, amount, currency, tags, account, recurrency }) {

        function makeGenerator(recurrencyToken, dates) {
            function *generateIncomes(accountToken) {
                for (const operationDate of dates) {
                    const data = {
                        type: Movement.TYPE_CREDIT,
                        accountToken,
                        recurrencyToken,
                        operationDate,
                        amount,
                        currency,
                        tags: expandTags(tags)
                    };
                    yield new Movement(data);
                }
            }
            return account.provideMovementData(generateIncomes);
        }

        return recurrency.provideMovementData(startDate, makeGenerator);
    }
}

module.exports = Factory;
