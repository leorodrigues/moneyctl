const moment = require('moment');
const { assistedClonning } = require('../clonning');
const { spreadAttributeValues } = require('./support');

const FIELDS = [
    '_id',
    'token',
    'name',
    'tags',
    'amount',
    'currency',
    'operationDate',
    'recurrencyToken',
    'assigneeAccountToken',
    'draweeAccountToken',
];

const MOVEMENT_FIELDS = [
    'token',
    'draweeAccountToken',
    'assigneeAccountToken',
    'currency',
    'amount',
    'tags',
    'operationDate',
    'recurrencyToken'
];

class Transfer {
    constructor(data = { }) {
        assistedClonning(data, undefined, FIELDS, this);
    }

    get serializable() {
        function momentToDate(momentValue, updateField) {
            updateField(momentValue && momentValue.toDate());
        } 
        function register(e) {
            e.on('operationDate', momentToDate);
        }
        return assistedClonning(this, register, FIELDS);
    }

    *iterateFragments() {
        yield { frag: 'id', value: this._id };
        yield { frag: 'operationDate', value: this.operationDate };
        yield { frag: 'token', value: this.token };
        yield { frag: 'tags', value: this.tags };
        yield { frag: 'currency', value: this.currency };
        yield { frag: 'amount', value: this.amount };
        yield { frag: 'drawee', value: this.draweeAccountToken };
        yield { frag: 'assignee', value: this.assigneeAccountToken };
    }

    provideMovementData(callback) {
        return callback(...spreadAttributeValues(this, MOVEMENT_FIELDS));
    }

    static desserialize(data) {
        data.operationDate = moment(data.operationDate);
        return new Transfer(data);
    }
}

module.exports = Transfer;
