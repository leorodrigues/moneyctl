const { assistedClonning } = require('../clonning');
const { spreadAttributeValues } = require('./support');

const FIELDS = [
    '_id', 'name', 'type', 'openingBalance', 'billing', 'token'
];

const BILL_DATA = [
    'period', 'periodLength', 'closureDay', 'deadlineDistance', 'isExtraPeriod'
];

const _TYPE_CREDIT = 'credit';

const _TYPE_CURRENT = 'current';

const _PERIOD_MONTH = 'month';

const _PERIOD_WEEK = 'week';

function computeBillingDateByWeek(billOpeningDate, periodLength) {
    let result = billOpeningDate.clone();
    result = result.startOf('week');
    result = result.add(periodLength, 'week');
    result = result.endOf('date');
    return result;
}

function computeBillingDateByMonth(
    billOpeningDate, periodLength, closureDay, isExtraPeriod) {
    const result = billOpeningDate.clone().date(closureDay);
    periodLength = isExtraPeriod ? periodLength : periodLength - 1;
    return result.add(periodLength, 'month').endOf('date');
}

class Account {
    constructor(data = { }) {
        assistedClonning(data, undefined, FIELDS, this);
    }

    get serializable() {
        return assistedClonning(this, undefined, FIELDS);
    }

    appendToSequence(sequencer) {
        sequencer.onAssigneeAccount(this.token).saveAccount();
    }

    computeBillingDate(billOpeningDate) {
        const { billing } = this;
        if (!billing) return billOpeningDate;
        const { period, periodLength, } = billing;
        if (Account.isWeekly(period))
            return computeBillingDateByWeek(billOpeningDate, periodLength);
        const { closureDay, isExtraPeriod } = billing;
        return computeBillingDateByMonth(
            billOpeningDate, periodLength, closureDay, isExtraPeriod);
    }

    appendOpeningBalance(balance) {
        balance.onAccount(this.token).setOpeningBalance(this.openingBalance);
    }

    impersonate(anotherInstance) {
        this._id = anotherInstance._id;
    }

    *iterateFragments() {
        yield { frag: 'id', value: this._id };
        yield { frag: 'name', value: this.name };
        yield { frag: 'opening', value: this.openingBalance };
        yield { frag: 'token', value: this.token };
        yield { frag: 'type', value: this.type };
    }

    provideQuestionsDefaults(assignField) {
        assignField('name', this.name);
        assignField('type', this.type);
        assignField('openingBalance', this.openingBalance);
        if (this.billing) {
            const {
                isExtraPeriod,
                period,
                periodLength,
                closureDay,
                deadlineDistance
            } = this.billing;
            assignField('billingPeriod', period);
            assignField('billingPeriodLength', periodLength);
            assignField('billingClosureDay', closureDay);
            assignField('billingDeadlineDistance', deadlineDistance);
            assignField('isExtraPeriod', isExtraPeriod);
        }
    }

    provideBillData(openingDate, callback) {
        return callback(
            ...spreadAttributeValues(this, BILL_DATA),
            this.token, openingDate, this.computeBillingDate(openingDate));
    }

    provideMovementData(callback) {
        return callback(this.token);
    }

    provideMovementLineData(callback) {
        return callback(this.token);
    }

    provideBindingData(callback) {
        return callback(this.token);
    }

    provideTransferData(callback) {
        return callback(this.token);
    }

    static get TYPE_CREDIT() {
        return _TYPE_CREDIT;
    }

    static get TYPE_CURRENT() {
        return _TYPE_CURRENT;
    }

    static isCredit(type) {
        return _TYPE_CREDIT === type;
    }

    static isCurrent(type) {
        return _TYPE_CURRENT === type;
    }

    static isMonthly(period) {
        return _PERIOD_MONTH === period;
    }

    static isWeekly(period) {
        return _PERIOD_WEEK === period;
    }

    static desserialize(data) {
        return new Account(data);
    }
}

module.exports = Account;
