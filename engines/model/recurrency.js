const { assistedClonning } = require('../clonning');
const { generateDateSequence } = require('./support');

const FIELDS = [
    '_id',
    'type',
    'token',
    'times',
    'period',
    'periodLength'
];

const _PERIOD_MONTH = 'month';

const _PERIOD_WEEK = 'week';

class Recurrency {
    constructor(data = { }) {
        assistedClonning(data, undefined, FIELDS, this);
    }

    get serializable() {
        return assistedClonning(this, undefined, FIELDS);
    }

    *iterateFragments() {
        yield { frag: 'id', value: this._id };
        yield { frag: 'type', value: this.type };
        yield { frag: 'times', value: this.times };
        yield { frag: 'periodLength', value: this.periodLength };
        yield { frag: 'period', value: this.period };
    }

    provideMovementData(startDate, callback) {
        const { times, period, periodLength: length } = this;
        const dates = generateDateSequence(startDate, times, period, length);
        return callback(this.token, dates);
    }

    static isMonthly(period) {
        return period === _PERIOD_MONTH;
    }

    static isWeekly(period) {
        return period === _PERIOD_WEEK;
    }

    static get PERIOD_MONTH() {
        return _PERIOD_MONTH;
    }

    static get PERIOD_WEEK() {
        return _PERIOD_WEEK;
    }

    static desserialize(data) {
        return new Recurrency(data);
    }
}

module.exports = Recurrency;