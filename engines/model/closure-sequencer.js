const { Writable } = require('stream');

module.exports = class ClosureSequencer {
    constructor() {
        const sequence = [ ];
        const assigneeAccounts = { };
        const onAssigneeAccount =
            handleOnAssigneeAccount(assigneeAccounts, sequence);
        Object.assign(this, { sequence, assigneeAccounts, onAssigneeAccount });
    }

    asyncPipeAndPlaceBinding(sequenciableStream) {
        const sequencer = this;
        return new Promise(resolve => {
            sequenciableStream.pipe(new Writable({
                objectMode: true,
                write(binding, enc, callback) {
                    binding.appendToSequence(sequencer);
                    callback();
                },
                final(callback) {
                    callback();
                    resolve();
                }
            }));
        });
    }

    *iterateClosures(factory) {
        let order = 0;
        const { assigneeAccounts } = this;
        for (const token of this.sequence) {
            yield makeClosure(factory, token, order, assigneeAccounts[token]);
            order++;
        }
    }
};

function handleSaveBinding(sequence, draweeList, assigneeToken, draweeToken) {
    return function saveBinding(token, name, tags) {
        draweeList.push({ bindingToken: token, name, tags, draweeToken });
        placeTokens(sequence, draweeToken, assigneeToken);
    };
}

function handleOnDraweeAccount(assigneeAccounts, sequence, assigneeToken) {
    return function onDraweeAccount(draweeToken) {
        const draweeList = getFromMap(assigneeAccounts, assigneeToken, []);
        const saveBinding = handleSaveBinding(
            sequence, draweeList, assigneeToken, draweeToken);
        return { saveBinding };
    };
}

function handleOnAssigneeAccount(assigneeAccounts, sequence) {
    return function onAssigneeAccount(assigneeToken) {
        const onDraweeAccount = handleOnDraweeAccount(
            assigneeAccounts, sequence, assigneeToken);
        return {
            onDraweeAccount,
            saveAccount() {
                if (!(assigneeToken in assigneeAccounts))
                    assigneeAccounts[assigneeToken] = { drawees: [] };
                if (sequence.indexOf(assigneeToken) > -1) return;
                sequence.push(assigneeToken);
            }
        };
    };
}

function placeTokens(sequence, draweeToken, assigneeToken) {
    const assigneeIndex = sequence.indexOf(assigneeToken);
    const draweeIndex = sequence.indexOf(draweeToken);
    if (draweeIndex > -1) {
        if (assigneeIndex === -1)
            sequence.splice(draweeIndex, 0, assigneeToken);
        return;
    }
    if (draweeIndex === -1 && assigneeIndex > -1) {
        sequence.splice(assigneeIndex + 1, 0, draweeToken);
        return;
    }
    sequence.unshift(draweeToken);
    sequence.unshift(assigneeToken);
}

function makeClosure(factory, assigneeAccountToken, order, drawees) {
    return factory.makeClosure({ assigneeAccountToken, order, drawees });
}

function getFromMap(map, key, defaultValue) {
    if (key in map) return map[key];
    map[key] = defaultValue;
    return defaultValue;
}