const moment = require('moment');
const { spawnDatesInInterval } = require('./support');

module.exports = class Activity {
    constructor(...activitySpans) {
        Object.assign(this, { activitySpans });
    }

    get serializable() {
        return this.activitySpans.map(s => ({
            startDate: s.startDate.toDate(),
            amount: s.amount,
            period: s.period,
            length: s.length
        }));
    }

    *iterateAmountByDate(startDate, endDate) {
        for (const span of this.activitySpans)
            for (const date of iterateDates(span, startDate, endDate))
                yield { date, amount: span.amount };
    }

    static desserialize(data) {
        data.forEach(i => i.startDate = moment(i.startDate));
        return new Activity(...data);
    }
};

function outOfInterval(activitySpan, startDate, endDate) {
    if (activitySpan.startDate.isAfter(endDate)) return true;
    return activitySpan.endDate && activitySpan.endDate.isBefore(startDate);
}

function selectLatest(subjectDate, referenceDate) {
    if (!subjectDate) return referenceDate;
    return referenceDate.isAfter(subjectDate) ? referenceDate : subjectDate;
}

function selectEarliest(subjectDate, referenceDate) {
    if (!subjectDate) return referenceDate;
    return referenceDate.isAfter(subjectDate) ? subjectDate : referenceDate;
}

function *iterateDates(activitySpan, startDate, endDate) {
    if (outOfInterval(activitySpan, startDate, endDate)) return;
    startDate = selectLatest(activitySpan.startDate, startDate);
    endDate = selectEarliest(activitySpan.endDate, endDate);
    const day = activitySpan.startDate.date();
    const { period, length } = activitySpan;
    yield *spawnDatesInInterval(startDate, endDate, day, period, length);
}
