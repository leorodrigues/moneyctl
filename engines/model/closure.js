const { assistedClonning } = require('../clonning');

const Movement = require('./movement');

const FIELDS = [
    '_id',
    'order',
    'drawees',
    'assigneeAccountToken',
];

module.exports = class Closure {
    constructor(data = { }) {
        assistedClonning(data, undefined, FIELDS, this);
    }

    get serializable() {
        return assistedClonning(this, undefined, FIELDS);
    }

    closeBalance(balance) {
        const account = balance.onAccount(this.assigneeAccountToken);

        for (const baseDate of balance.iterateDates()) {
            const month = account.onDate(baseDate);

            if (this.drawees.length) {
                const amount = month.previewBalance();
                forwardDrawees(
                    this.drawees,
                    this.assigneeAccountToken,
                    balance,
                    amount,
                    baseDate);
            }

            month.closeBalance();
        }
    }

    static desserialize(data) {
        return new Closure(data);
    }
};

function forwardDrawees(drawees, assigneeToken, balance, amount, baseDate) {
    if (amount >= 0) return;
    for (const drawee of drawees)
        appendMovements(balance, assigneeToken, drawee, amount, baseDate);
}

function appendMovements(balance, assigneeToken, drawee, amount, baseDate) {
    amount = (drawee.factor || 1) * Math.abs(amount);

    balance.onAccount(assigneeToken)
        .onDate(baseDate)
        .onMovementType(Movement.TYPE_CREDIT)
        .aggregate(amount, drawee.tags, drawee.bindingToken);

    balance.onAccount(drawee.draweeToken)
        .onDate(baseDate.clone().add(1, 'month'))
        .onMovementType(Movement.TYPE_DEBIT)
        .aggregate(amount, drawee.tags, drawee.bindingToken);

    return amount;
}