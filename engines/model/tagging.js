function distribute(pairs, predicates, name, value, predicate) {
    if (!predicate) {
        pairs[name] = value;
        return;
    }
    predicates.push(predicate);
}

function expandTags(tagsString) {
    const regex = /([^:;]+):([^:;]+);|([^:;]+);/g;
    const result = {};
    const predicates = [];
    tagsString = `${tagsString};`.replace(/\s*(:|;)\s*/g, (i, g) => g);
    let match;
    while ((match = regex.exec(tagsString))) {
        const [, name, value, predicate] = match;
        distribute(result, predicates, name, value, predicate);
    }
    result.predicates = predicates;
    return result;
}

function compressTags(tags) {
    function squash(list, next) {
        if (next === 'predicates')
            list.push(...tags[next]);
        else
            list.push(`${next}:${tags[next]}`);
        return list;
    }
    return Object.keys(tags).reduce(squash, []).join(';');
}

module.exports = { expandTags, compressTags };