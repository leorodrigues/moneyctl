const moment = require('moment');
const { assistedClonning } = require('../clonning');
const { spreadAttributeValues } = require('./support');

const FIELDS = [
    '_id',
    'amount',
    'type',
    'currency',
    'tags',
    'operationDate',
    'billingDate',
    'lineToken',
    'billingToken',
    'accountToken',
    'transferToken',
    'recurrencyToken'
];

const DEFAULTS_FIELDS = [
    'operationDate', 'accountToken', 'amount', 'currency', 'tags'
];

const _TYPE_CREDIT = 'credit';
const _TYPE_DEBIT = 'debit';

class Movement {
    constructor(data = { }) {
        assistedClonning(data, undefined, FIELDS, this);
    }

    get effectiveDate() {
        if (this.billingDate) return this.billingDate;
        return this.operationDate;
    }

    get serializable() {
        function momentToDate(momentValue, updateField) {
            updateField(momentValue && momentValue.toDate());
        }
        function register(e) {
            e.on('operationDate', momentToDate);
            e.on('billingDate', momentToDate);
        }
        return assistedClonning(this, register, FIELDS);
    }

    appendToBalance(balance) {
        balance.onAccount(this.accountToken)
            .onDate(this.effectiveDate)
            .onMovementType(this.type)
            .aggregate(this.amount, this.tags, this.lineToken);
    }

    impersonate(anotherInstance) {
        this._id = anotherInstance._id;
    }

    *iterateFragments() {
        yield { frag: 'id', value: this._id };
        yield { frag: 'amount', value: this.amount };
        yield { frag: 'currency', value: this.currency };
        yield { frag: 'operationDate', value: this.operationDate };
        yield { frag: 'account', value: this.accountToken };
        yield { frag: 'tags', value: this.tags };
    }

    provideQuestionDefaults(callback) {
        return callback(...spreadAttributeValues(this, DEFAULTS_FIELDS));
    }

    static get TYPE_CREDIT() {
        return _TYPE_CREDIT;
    }

    static get TYPE_DEBIT() {
        return _TYPE_DEBIT;
    }

    static isDebit(type) {
        return type === _TYPE_DEBIT;
    }

    static isCredit(type) {
        return type === _TYPE_CREDIT;
    }

    static desserialize(data) {
        data.operationDate = moment(data.operationDate);
        if (data.billingDate)
            data.billingDate = moment(data.billingDate);
        return new Movement(data);
    }
}

module.exports = Movement;
