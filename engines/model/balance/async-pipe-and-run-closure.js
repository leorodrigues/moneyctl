const { Writable } = require('stream');

module.exports = class AsyncPipeAndRunClosure {
    invoke(closureStream, balance) {
        return new Promise(resolve => {
            closureStream.pipe(new Writable({
                objectMode: true,
                write(object, enc, callback) {
                    object.closeBalance(balance);
                    callback();
                },
                final(callback) {
                    callback();
                    resolve();
                }
            }));
        });
    }
};
