const { Writable } = require('stream');

module.exports = class AsyncPipeAndCollectOpeningBalance {
    invoke(accountStream, balance) {
        return new Promise(resolve => {
            accountStream.pipe(new Writable({
                objectMode: true,
                write(object, enc, callback) {
                    object.appendOpeningBalance(balance);
                    callback();
                },
                final(callback) {
                    callback();
                    resolve();
                }
            }));
        });
    }
};
