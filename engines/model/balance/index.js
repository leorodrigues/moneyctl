const { spawnDatesInInterval } = require('../support');

const AsyncPipeAndCollectOpeningBalance =
    require('./async-pipe-and-collect-opening-balance');

const AsyncPipeAndAppendToBalance =
    require('./async-pipe-and-append-to-balance');

const AsyncPipeAndRunClosure =
    require('./async-pipe-and-run-closure');

const _asyncPipeAndCollectOpeningBalance =
    new AsyncPipeAndCollectOpeningBalance();

const _asyncPipeAndAppendToBalance =
    new AsyncPipeAndAppendToBalance();

const _asyncPipeAndRunClosure =
    new AsyncPipeAndRunClosure();

module.exports = class Balance {
    constructor(startDate, endDate) {
        const data = { };
        const dates = makeDates(startDate, endDate);
        Object.assign(this, {
            data,
            startDate,
            endDate,
            dates,
            onAccount: handleOnAccount(data)
        });
    }

    asyncPipeAndAppend(appendableStream) {
        return _asyncPipeAndAppendToBalance.invoke(appendableStream, this);
    }

    asyncPipeAndCollectOpeningBalance(accountStream) {
        return _asyncPipeAndCollectOpeningBalance.invoke(accountStream, this);
    }

    asyncPipeAndRunClosure(closureStream) {
        return _asyncPipeAndRunClosure.invoke(closureStream, this);
    }

    underTimeInterval(callback) {
        return callback(this.startDate.clone(), this.endDate.clone());
    }

    *iterateDates() {
        for (const d of this.dates)
            yield d.clone();
    }

    *iterateDateFragments() {
        for (const d of this.dates)
            yield { value: d.clone(), frag: 'date' };
    }

    *iterateAccountFragments() {
        for (const account in this.data)
            yield { value: account, frag: 'account' };
    }

    *iterateSummaryFragments(date, account) {
        account = account.value;
        date = date.value.format('YYYY-MM');
        const monthData = this.data[account][date];
        if (!monthData) return;
        const { balance, netIncome, debit, credit } = monthData;
        yield { frag: 'dataPoint', name: 'balance', value: balance };
        yield { frag: 'dataPoint', name: 'netIncome', value: netIncome };
        yield { frag: 'dataPoint', name: 'totalDebit', value: debit.total };
        yield { frag: 'dataPoint', name: 'totalCredit', value: credit.total };
    }

    *iterateCreditLineFragments(date, account) {
        account = account.value;
        date = date.value.format('YYYY-MM');
        const monthData = this.data[account][date];
        if (!monthData) return;
        const { credit: { lines } } = monthData;
        for (const line in lines)
            yield { frag: 'dataPoint', name: line,  value: lines[line] };
    }

    *iterateDebitLineFragments(date, account) {
        account = account.value;
        date = date.value.format('YYYY-MM');
        const monthData = this.data[account][date];
        if (!monthData) return;
        const { debit: { lines } } = monthData;
        for (const line in lines)
            yield { frag: 'dataPoint', name: line,  value: lines[line] };
    }
};

function getAttribute(map, key, initial) {
    if (key in map) return map[key];
    map[key] = initial;
    return initial;
}

function accumulate(data, key, value) {
    if (key in data) return data[key] += value;
    data[key] = value;
}

function accumulateTags(tags, givenTags, value) {
    for (const key in givenTags) {
        if (key === 'predicates')
            givenTags[key].forEach(p => accumulate(tags, p, value));
        else
            accumulate(tags, key, value);
    }
}

function rebalanceNetIncome(data) {
    const { debit, credit } = data;
    data.netIncome = credit.total - debit.total;
}

function handleOnMovementType(monthData) {
    return function onMovementType(type) {
        const typeData = getAttribute(monthData, type, { });
        return {
            aggregate(amount, givenTags, lineToken = 'unforseen') {
                const tags = getAttribute(typeData, 'tags', { });
                const lines = getAttribute(typeData, 'lines', { });
                accumulate(typeData, 'total', amount);
                accumulate(lines, lineToken, amount);
                accumulateTags(tags, givenTags, amount);
                rebalanceNetIncome(monthData);
            }
        };
    };
}

function handleOnDate(accountData) {
    return function onDate(date) {
        const prevMonthKey = date.clone()
            .subtract(1, 'month').format('YYYY-MM');
        const monthKey = date.format('YYYY-MM');
        const monthData = getMonthData(accountData, monthKey);
        return {
            onMovementType: handleOnMovementType(monthData),

            previewBalance() {
                const netIncome = monthData.netIncome;
                return prevMonthKey in accountData
                    ? accountData[prevMonthKey].balance + netIncome
                    : (accountData.openingBalance || 0) + netIncome;
            },

            closeBalance() {
                return monthData.balance = this.previewBalance();
            }
        };
    };
}

function handleOnAccount(data) {
    return function onAccount(accountToken) {
        const accountData = getAccountData(data, accountToken);
        return {
            onDate: handleOnDate(accountData),
            setOpeningBalance(openingBalance) {
                accountData.openingBalance = openingBalance || 0;
            }
        };
    };
}

function makeDates(startDate, endDate) {
    return [ ...spawnDatesInInterval(startDate, endDate, 1, 'month', 1) ];
}

function getAccountData(balanceData, accountToken) {
    if (balanceData.hasOwnProperty(accountToken))
        return balanceData[accountToken];
    const accountData = {
        lineTokens: []
    };
    balanceData[accountToken] = accountData;
    return accountData;
}

function getMonthData(accountData, monthKey) {
    if (accountData.hasOwnProperty(monthKey))
        return accountData[monthKey];
    const monthData = {
        debit: { tags: { }, lines: { }, total: 0 },
        credit: { tags: { }, lines: { }, total: 0 },
        netIncome: 0
    };
    accountData[monthKey] = monthData;
    return monthData;
}
