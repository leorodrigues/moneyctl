const { Writable } = require('stream');

module.exports = class AsyncPipeAndAppendToBalance {
    invoke(appendableStream, balance) {
        return new Promise(resolve => {
            appendableStream.pipe(new Writable({
                objectMode: true,
                write(object, enc, callback) {
                    object.appendToBalance(balance);
                    callback();
                },
                final(callback) {
                    callback();
                    resolve();
                }
            }));
        });
    }
};
