const moment = require('moment');
const Account = require('./account');
const Movement = require('./movement');
const Balance = require('./balance');
const ClosureSequencer = require('./closure-sequencer');

module.exports = class ATM {
    constructor(factory, dataAccessMaping) {
        Object.assign(this, dataAccessMaping, { factory });
    }

    async computeBalance(startDate, endDate) {
        const { closures, accounts, movements, movementLines: lines } = this;

        const balance = new Balance(startDate, endDate);

        await balance.asyncPipeAndCollectOpeningBalance(
            await accounts.streamAll());

        await balance.asyncPipeAndAppend(
            await lines.streamAllActiveBetweenDates(startDate, endDate));

        await balance.asyncPipeAndAppend(
            await movements.streamAllEffectiveBetweenDates(startDate, endDate));

        await balance.asyncPipeAndRunClosure(
            await closures.streamAll());

        return balance;
    }

    async makeTransfer(draweeAccount, assigneeAccount, data) {
        const transfer = await this.saveTransfer(
            draweeAccount, assigneeAccount, data);
        const income = await this.saveTransferIncome(transfer);
        const expense = await this.saveTransferExpense(transfer);
        return { transfer, income, expense };
    }

    async openAccount(data) {
        const { factory, accounts } = this;
        const { type } = data;
        
        const instance = Account.isCredit(type)
            ? factory.makeCreditAccount(data)
            : factory.makeCurrentAccount(data);
        
        const result = await accounts.insert(instance);
        await this.updateClosureSequence();
        return result;
    }

    async updateAccount(account, data) {
        const { factory, accounts } = this;
        const instance = factory.makeUpdatingAccountCopy(data, account);
        return await accounts.update(instance);
    }

    async bindAccounts(assigneeAccount, draweeAccount, bindingData) {
        const { factory, bindings } = this;
        const { name, tags } = bindingData;
        const data = { name, tags, draweeAccount, assigneeAccount };
        const instance = factory.makeBinding(data);
        
        const result = await bindings.insert(instance);
        await this.updateClosureSequence();
        return result;
    }

    async addBill(account) {
        const { factory, bills } = this;
        const openingDate = moment();
        if (await bills.existsOpenByAccountToken(account.token))
            throw new Error('There is already a bill for this account');
        return await bills.insert(factory.makeBill({ account, openingDate }));
    }

    async addExpense(data) {
        const { factory, movements } = this;
        return await movements.insert(factory.makeExpense(data));
    }

    async updateExpense(data, expense) {
        const { factory, movements } = this;
        const instance = factory.makeUpdatingIncomeCopy(data, expense);
        await movements.update(instance);
        return instance;
    }

    async addExpenseLine(data) {
        const { factory, movementLines } = this;
        data = this.collectMovementLineParameters(data, Movement.TYPE_DEBIT);
        return await movementLines.insert(factory.makeMovementLine(data));
    }

    async addIncome(data) {
        const { factory, movements } = this;
        return await movements.insert(factory.makeIncome(data));
    }

    async updateClosureSequence() {
        const { closures, bindins, accounts, factory } = this;
        const sequencer = new ClosureSequencer();

        for (const c of await closures.findAll())
            closures.deleteInstance(c);

        await sequencer.asyncPipeAndPlaceBinding(await bindins.streamAll());
        await sequencer.asyncPipeAndPlaceBinding(await accounts.streamAll());

        for (const c of sequencer.iterateClosures(factory))
            await closures.insert(c);
    }

    async updateIncome(data, income) {
        const { factory, movements } = this;
        const instance = factory.makeUpdatingIncomeCopy(data, income);
        await movements.update(instance);
        return instance;
    }

    async addIncomeLine(data) {
        const { factory, movementLines } = this;
        data = this.collectMovementLineParameters(data, Movement.TYPE_CREDIT);
        return await movementLines.insert(factory.makeMovementLine(data));
    }

    async addRecurrentExpense(data) {
        const { factory, movements } = this;
        const { times, period, periodLength, startDate } = data;
        const { account, amount, currency, tags } = data;
    
        const recurrency = await this.saveRecurrency(
            Movement.TYPE_DEBIT, times, period, periodLength);

        const instances = [...factory.generateExpensesFromRecurrency(
            { startDate, amount, currency, tags, account, recurrency })];

        const expenses = await Promise.all(instances.map(m =>
            movements.insert(m)));

        return { recurrency, expenses };
    }

    async addRecurrentIncome(data) {
        const { factory, movements } = this;
        const { times, period, periodLength, startDate } = data;
        const { account, amount, currency, tags } = data;
    
        const recurrency = await this.saveRecurrency(
            Movement.TYPE_CREDIT, times, period, periodLength);

        const instances = [...factory.generateIncomesFromRecurrency(
            { startDate, amount, currency, tags, account, recurrency })];

        const incomes = await Promise.all(instances.map(m =>
            movements.insert(m)));

        return { recurrency, incomes };
    }

    async findAllAccounts() {
        return this.accounts.findAll();
    }

    async findAccountById(accountId) {
        return this.accounts.findById(accountId);
    }

    async findAccountByToken(accountToken) {
        return this.accounts.findByToken(accountToken);
    }

    async findBindingById(bindingId) {
        return this.bindings.findById(bindingId);
    }

    async findMovementById(movementId) {
        return this.movements.findById(movementId);
    }

    async findIncomeLineById(lineId) {
        return this.movementLines.findIncomeByIf(lineId);
    }

    async findExpenseLineById(lineId) {
        return this.movementLines.findExpenseById(lineId);
    }

    async findRecurrencyById(recurrencyId) {
        return this.recurrencies.findById(recurrencyId);
    }

    async findTransferById(transferId) {
        return this.transfers.findById(transferId);
    }

    async deleteAccountById(accountId) {
        const result = await this.accounts.deleteById(accountId);
        await this.updateClosureSequence();
        return result;
    }

    async deleteBindingById(bindingId) {
        const result = this.bindings.deleteById(bindingId);
        await this.updateClosureSequence();
        return result;
    }

    async deleteMovementById(movementId) {
        return this.movements.deleteById(movementId);
    }

    async deleteMovementByLineToken(token) {
        await this.movements.deleteByLineToken(token);
    }

    async deleteMovementLineInstance(movementLine) {
        await this.deleteMovementByLineToken(movementLine.token);
        return this.movementLines.deleteInstance(movementLine);
    }

    async deleteRecurrencyInstance(recurrency) {
        const { movements, recurrencies } = this;
        await movements.deleteByRecurrencyToken(recurrency.token);
        return recurrencies.deleteInstance(recurrency);
    }

    async deleteTransferInstance(transfer) {
        const { transfers, movements } = this;
        await transfer.provideMovementData(token =>
            movements.deleteByTransferToken(token));
        return transfers.deleteInstance(transfer);
    }

    streamAllAccounts() {
        const { accounts } = this;
        return accounts.streamAll();
    }

    streamAllBindings() {
        const { bindings } = this;
        return bindings.streamAll();
    }

    streamAllIncomes() {
        const { movements } = this;
        return movements.streamAllIncomes();
    }

    streamAllIncomeLines() {
        const { movementLines } = this;
        return movementLines.streamAllIncomes();
    }

    streamAllExpenses() {
        const { movements } = this;
        return movements.streamAllExpenses();
    }

    streamAllExpenseLines() {
        const { movementLines } = this;
        return movementLines.streamAllExpenses();
    }

    streamAllRecurrencies() {
        const { recurrencies } = this;
        return recurrencies.streamAll();
    }

    streamAllTransfers() {
        const { transfers } = this;
        return transfers.streamAll();
    }

    saveRecurrency(type, times, period, periodLength) {
        const { factory, recurrencies } = this;
        return recurrencies.insert(factory.makeRecurrency(
            { type, times, period, periodLength }));
    }

    saveTransfer(draweeAccount, assigneeAccount, data) {
        const { factory, transfers } = this;
        return transfers.insert(factory.makeTransfer(
            Object.assign({ draweeAccount, assigneeAccount }, data)));
    }

    saveTransferExpense(transfer) {
        const { factory, movements } = this;
        return movements.insert(factory.makeTransferExpense({ transfer }));
    }
    
    saveTransferIncome(transfer) {
        const { factory, movements } = this;
        return movements.insert(factory.makeTransferIncome({ transfer }));
    }

    collectMovementLineParameters(data, type) {
        const {
            name, account, amount, currency, tags, startDate, length, period
        } = data;
        return {
            name, account, amount, currency, type, tags,
            length, period, startDate
        };
    }
};