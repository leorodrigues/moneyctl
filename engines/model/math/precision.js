function roundUp(scale = 6) {
    return n => (Math.round(n + 'e+' + scale)  + 'e-' + scale);
}

function sum(scale = 6) {
    const roundUpFn = roundUp(scale);
    return function(...terms) {
        return roundUpFn(terms.map(roundUpFn).reduce((a, p) => a + p, 0));
    };
}

module.exports = {sum, roundUp};
