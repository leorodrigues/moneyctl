const moment = require('moment');

function makeTokenFromName(accountName) {
    return accountName
        .replace(/[^\w]/g, '-')
        .replace(/-+/g, '-')
        .toLocaleLowerCase();
}

function *spreadAttributeValues(object, attributeNames) {
    for (const k of attributeNames)
        yield object[k];
}

function *generateDateSequence(startDate, size, period, periodLength) {
    let currentDate = moment(startDate);
    for (let i = 0; i < size; i++) {
        yield currentDate;
        currentDate = currentDate.clone();
        currentDate.add(periodLength, period);
    }
}

function *spawnDatesInInterval(
    startDate, endDate, placementDay, period, periodLength) {

    let current = moment(startDate).date(placementDay);
    do {
        yield current;
        current = current.clone().add(periodLength, period);
    } while (current.isBetween(startDate, endDate));
}

module.exports = {
    spawnDatesInInterval,
    makeTokenFromName,
    spreadAttributeValues,
    generateDateSequence
};
