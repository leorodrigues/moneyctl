
function forward(pending, handlers) {
    let offset = 0;
    return () => {
        if (offset < handlers.length && pending.length === 0)
            pending.push(handlers[offset++]);
    };
}

async function follow(context, ...handlers) {
    const pending = [];
    const nextFn = forward(pending, handlers, 1);
    nextFn();
    while (pending.length > 0) {
        const handler = pending.pop();
        await handler(context, nextFn);
    }
}

module.exports = {follow};