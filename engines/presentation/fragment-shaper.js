const { Transform } = require('stream');

module.exports = class FragmentStreamer {

    constructor() {
        const shapes = [ ];
        Object.assign(this, { shapes });
    }

    shape(name, width = 12, pad = 1, align = 'left', fill = ' ') {
        this.shapes.push({ name, width, pad, align, fill });
        return this;
    }

    output() {
        const { shapes } = this;
        return new Transform({
            objectMode: true,
            transform(object, enc, callback) {
                this.push(handleFragment(object, shapes));
                callback();
            }
        });
    }

    static computeColumns(screenSize, ...preferences) {
        const result = [ ];
        let remaining = screenSize;
        let autosize = 0;
        for (const p of preferences) {
            if (p >= 1) {
                result.push(p);
                remaining -= p;
            } else if (p <= 0) {
                result.push(p);
                autosize++;
            } else {
                const absolute = Math.round(screenSize * p);
                result.push(absolute);
                remaining -= p;
            }
        }
        if (autosize === 0)
            return result;
        const even = Math.round(remaining / autosize);
        for (let i = 0; i < result.length; i++) {
            if (result[i] === 0)
                result[i] = even;
        }
        return result;
    }
};

function handleFragment(object, shapes) {
    const shape = findShape(object, shapes);
    if (shape) {
        const { width, pad, align, fill } = shape;
        Object.assign(object, { width, pad, align, fill });
    }
    return object;
}

function findShape(object, shapes) {
    return shapes.find(s => s.name === object.frag);
}
