const { Transform } = require('stream');

module.exports = class FragmentParser {
    constructor() {
        const parsers = { };
        Object.assign(this, { parsers });
    }

    fragment(name, parseFn) {
        this.parsers[name] = parseFn;
        return this;
    }

    parse() {
        const { parsers } = this;
        return new Transform({
            objectMode: true,
            transform(object, enc, callback) {
                this.push(parse(object, parsers));
                callback();
            }
        });
    }
};

function parse(object, parsers) {
    const fn = findParser(object.frag, parsers);
    if (fn)
        object.text = fn instanceof Function ? fn(object) : fn;
    return object;
}

function findParser(frag, parsers) {
    for (const k in parsers)
        if (k === frag)
            return parsers[k];
    return null;
}
