
const TableFlow = require('./table-flow');
const FragmentParser = require('./fragment-parser');
const FragmentShaper = require('./fragment-shaper');
const FragmentStyler = require('./fragment-styler');

module.exports = class ToolsKit {
    makeShaper() {
        return new FragmentShaper();
    }

    makeParser() {
        return new FragmentParser();
    }

    makeStyler() {
        return new FragmentStyler();
    }

    makeTableFlow() {
        return new TableFlow();
    }
};