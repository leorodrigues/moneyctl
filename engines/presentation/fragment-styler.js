const { Transform } = require('stream');

module.exports = class FragmentStyler {
    constructor() {
        const stylesByFragment = { };
        Object.assign(this, { stylesByFragment });
    }

    styleFor(name, style) {
        this.stylesByFragment[name] = style;
        return this;
    }

    stylize() {
        const { stylesByFragment } = this;
        return new Transform({
            objectMode: true,
            transform(object, enc, callback) {
                this.push(stylize(object, stylesByFragment));
                callback();
            }
        });
    }
};

function stylize(object, styles) {
    const style = styles[object.frag];
    if (style) object.style = style;
    return object;
}

