const { Transform } = require('stream');

module.exports = class RecordFragmenter {
    fragment() {
        return new Transform({
            objectMode: true,
            transform(record, enc, callback) {
                for (const fragment of record.iterateFragments())
                    this.push(fragment);
                callback();
            }
        });
    }
};
