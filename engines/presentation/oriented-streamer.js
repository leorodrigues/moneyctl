const { Readable } = require('stream');

module.exports = class OrientedStreamer {
    constructor(minScreenWidth, horizontalStreamer, verticalStreamer) {
        Object.assign(this, {
            minScreenWidth, horizontalStreamer, verticalStreamer
        });
    }

    streamInstances(...instances) {
        return new Readable({
            objectMode: true,
            read() {
                for (const instance of instances)
                    this.push(instance);
                this.push(null);
            }
        });
    }

    presentTable(tableStream, tableName) {
        const { minScreenWidth, horizontalStreamer, verticalStreamer } = this;
        return process.stdout.columns >= minScreenWidth
            ? horizontalStreamer.presentTable(tableStream, tableName)
            : verticalStreamer.presentTable(tableStream, tableName);
    }
};