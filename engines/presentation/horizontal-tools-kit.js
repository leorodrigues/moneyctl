
const ToolsKit = require('./tools-kit');
const HorizontalTableFlow = require('./horizontal-table-flow');

module.exports = class HorizontalToolsKit extends ToolsKit {
    makeTableFlow() {
        return new HorizontalTableFlow();
    }
};