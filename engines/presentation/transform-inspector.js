
module.exports = class TransformInspector {

    constructor(delegate, transformations = {}) {
        Object.assign(this, { transformations, delegate });
    }

    inspect(object, fieldName) {
        const { transformations, delegate } = this;
        const proxy = delegate.inspect(object, fieldName);
        const target = { proxy, transformations };
        return new Proxy(target, HANDLERS);
    }
};

const HANDLERS = {
    get: function(target, property) {
        const { proxy, transformations, lastProperty } = target;

        if (property === 'value') {
            const value = Reflect.get(proxy, property);
            if (lastProperty in transformations)
                return transformations[lastProperty][value] || value;
            return value;
        }

        target = {
            transformations,
            lastProperty: property,
            proxy: Reflect.get(proxy, property)
        };

        return new Proxy(target, HANDLERS);
    }
};
