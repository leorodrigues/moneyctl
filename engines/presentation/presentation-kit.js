
const RecordFragmenter =
    require('./record-fragmenter');

const HierarchyInspector =
    require('./hierarchy-inspector');

const TransformInspector =
    require('./transform-inspector');

const HorizontalToolsKit = 
    require('./horizontal-tools-kit');

const VerticalToolsKit = 
    require('./vertical-tools-kit');

const OrientedStreamer = 
    require('./oriented-streamer');

const LoadElementConfig =
    require('./load-element-config');

const PresentationStreamer =
    require('./presentation-streamer');

const Planner = require('./planner');

module.exports = class PresentationKit {

    makeFragmenter() {
        return new RecordFragmenter();
    }

    makeConfigInspector(transformations) {
        return new TransformInspector(
            new HierarchyInspector(), transformations);
    }

    makeHorizontalStreamer(fragmenter, inspector, config) {
        const planner = new Planner(new HorizontalToolsKit(), [
            new LoadElementConfig('horizontal', 'head', 'head'),
            new LoadElementConfig('horizontal', 'cell')
        ]);
        return new PresentationStreamer(fragmenter, inspector, planner)
            .configure(config);
    }

    makeVerticalStreamer(fragmenter, inspector, config) {
        const planner = new Planner(new VerticalToolsKit(), [
            new LoadElementConfig('vertical', 'label', 'label'),
            new LoadElementConfig('vertical', 'cell')
        ]);
        return new PresentationStreamer(fragmenter, inspector, planner)
            .configure(config);
    }

    makeOrientedStreamer(minScreenWidth, horizontalStreamer, verticalStreamer) {
        return new OrientedStreamer(
            minScreenWidth, horizontalStreamer, verticalStreamer);
    }
};