
module.exports = class LoadElementConfig {
    constructor(orientation, element, elementQualifier) {
        Object.assign(this, { orientation, element, elementQualifier });
    }
    
    invoke(shaper, styler, parser, fieldName, field) {
        const { orientation, element, elementQualifier } = this;
        const orientationElmnt = Reflect.get(field, orientation);
        const elementReference = Reflect.get(orientationElmnt, element);
    
        const width = elementReference.width.value;
        const transform = elementReference.transform.value;
        const align = elementReference.align.value;
        const style = elementReference.style.value;

        const frag = this.computeFrag(fieldName, elementQualifier);
    
        if (transform)
            parser.fragment(frag, transform);

        if (style)
            styler.styleFor(frag, style);

        shaper.shape(frag, width, 1, align);
    }

    computeFrag(fieldName, elmntQualifier) {
        return !elmntQualifier ? fieldName : `${fieldName}:${elmntQualifier}`;
    }
};