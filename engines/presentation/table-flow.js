
const { Transform } = require('stream');
const { NEW_LINE } = require('./constants');

module.exports = class TableFlow {
    constructor() {
        const columns = [ ];
        Object.assign(this, { columns });
    }

    column(name) {
        this.columns.push(name);
        return this;
    }

    flowHorizontaly() {
        const context = { line: [ ], columns: this.columns };
        const outstandingElements = [...makeHeader(this.columns)];
        return new Transform({
            objectMode: true,
            transform(fragment, enc, callback) {
                for (const f of horizontalFlow(fragment, context)) {
                    while (outstandingElements.length)
                        this.push(outstandingElements.shift());
                    this.push(f);
                }
                callback();
            },
            final(callback) {
                this.push(NEW_LINE);
                callback();
            }
        });
    }

    flowVerticaly() {
        const context = { line: [ ], columns: this.columns };
        const outstandingElements = [ ];
        return new Transform({
            objectMode: true,
            transform(fragment, enc, callback) {
                for (const f of verticalFlow(fragment, context)) {
                    while (outstandingElements.length)
                        this.push(outstandingElements.shift());
                    this.push(f);
                }
                callback();
            },
            final(callback) {
                this.push(NEW_LINE);
                callback();
            }
        });
    }
};

function *makeHeader(columns) {
    for (const column of columns)
        yield { frag: `${column}:head`, value: column };
    yield NEW_LINE;
}

function *horizontalFlow(fragment, { line, columns }) {
    if (collect(line, columns, fragment))
        yield *flush(line, columns);
}

function *verticalFlow(fragment, { line, columns }) {
    if (collect(line, columns, fragment))
        yield *splitFlush(line, columns);
}

function collect(line, columns, fragment) {
    if (line.some(byFrag(fragment.frag)))
        return true;
    if (columns.indexOf(fragment.frag) > -1)
        line.push(fragment);
    return line.length === columns.length;
}

function *flush(line, columns) {
    for (const column of columns) {
        const fragment = line.find(f => column === f.frag);
        if (!fragment)
            yield { frag: column, isEmpty: true };
        else
            yield fragment;
    }
    line.length = 0;
    yield NEW_LINE;
}

function *splitFlush(line, columns) {
    for (const column of columns) {
        yield { frag: `${column}:label`, column };
        const fragment = line.find(f => column === f.frag);
        if (!fragment)
            yield { frag: column, isEmpty: true };
        else
            yield fragment;
        yield NEW_LINE;
    }
    line.length = 0;
    yield NEW_LINE;
}

function byFrag(frag) {
    return f => frag === f.frag;
}
