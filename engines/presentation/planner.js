

module.exports = class Planner {
    constructor(presentationKit, loadCommands) {
        Object.assign(this, { presentationKit, loadCommands });
    }

    makeTools(fieldList, fieldSpecs, inspector) {
        const { presentationKit, loadCommands } = this;

        const tableFlow = presentationKit.makeTableFlow();
        const shaper = presentationKit.makeShaper();
        const parser = presentationKit.makeParser();
        const styler = presentationKit.makeStyler();

        for (const { ref: fieldName } of fieldList) {
            tableFlow.column(fieldName);
            const field = inspector.inspect(fieldSpecs, fieldName);

            for (const command of loadCommands)
                command.invoke(shaper, styler, parser, fieldName, field);
        }

        return { tableFlow, shaper, parser, styler };
    }
};

