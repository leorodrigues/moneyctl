const os = require('os');
const { Readable } = require('stream');

const { NEW_LINE } = require('./constants');

const STYLES = require('./styles').byName();

const FragmentStyler = require('./fragment-styler');
const FragmentRenderer = require('./fragment-renderer');

const TITLE_STYLE = STYLES.fgCyan.value;
const VERSION_STYLE = STYLES.fgLightGreen.value;
const PLATFORM_STYLE = STYLES.fgLightBlack.value;

const DEF_STYLER = new FragmentStyler();
const DEF_RENDERER = new FragmentRenderer();

module.exports = class IntroBanner {
    constructor(renderer = DEF_RENDERER, styler = DEF_STYLER) {
        Object.assign(this, { renderer, styler });
        configurePresentation(styler);
    }

    asyncOutputBanner(pkgInfo, writeStream) {
        return new Promise(resolve => {
            const stream = streamOutFragments(pkgInfo);
            stream.on('end', resolve);
            stream.pipe(this.styler.stylize())
                .pipe(this.renderer.render())
                .pipe(writeStream);
        });
    }
};

function streamOutFragments(pkgInfo) {
    const i = iterateBanner(pkgInfo);
    return new Readable({
        objectMode: true,
        read() {
            const next = i.next();
            this.push(next.done ? null : next.value);
        }
    });
}

function *iterateBanner(packageInfo) {
    const { versions } = process;
    const title = packageInfo.description || 'Node App';
    yield { frag: 'title', value: title.toLocaleUpperCase() };
    yield { frag: 'appVersion', value: `v${packageInfo.version}` };
    yield NEW_LINE;
    yield NEW_LINE;
    yield { frag: 'os', value: `${os.type()} v${os.release()}` };
    yield { frag: 'platform', value: `${os.platform()}/${os.arch()}` };
    yield NEW_LINE;
    yield {
        frag: 'node', value: `Node ${versions.node}; V8 ${versions.v8}`
    };
    yield NEW_LINE;
    yield { frag: 'hostname', value: `@${os.hostname()}` };
    yield NEW_LINE;
    yield NEW_LINE;
    yield NEW_LINE;
}

function configurePresentation(styler) {
    styler.styleFor('title', TITLE_STYLE);
    styler.styleFor('appVersion', VERSION_STYLE);
    styler.styleFor('os', PLATFORM_STYLE);
    styler.styleFor('platform', PLATFORM_STYLE);
    styler.styleFor('node', PLATFORM_STYLE);
    styler.styleFor('hostname', PLATFORM_STYLE);
}
