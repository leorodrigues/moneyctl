const { Readable } = require('stream');

module.exports = class PresentationStreamer {

    constructor(fragmenter, inspector, planner) {
        const tools = { };
        Object.assign(this, { fragmenter, inspector, planner, tools });
    }

    streamInstances(...instances) {
        return new Readable({
            objectMode: true,
            read() {
                for (const instance of instances)
                    this.push(instance);
                this.push(null);
            }
        });
    }

    presentTable(tableStream, tableName) {
        const { fragmenter } = this;
        const { tableFlow, shaper, styler, parser } = this.tools[tableName];

        return tableStream
            .pipe(fragmenter.fragment())
            .pipe(tableFlow.flow())
            .pipe(parser.parse())
            .pipe(shaper.output())
            .pipe(styler.stylize());
    }

    configure(data) {
        const { tableSpecs, fieldSpecs } = data;
        const { inspector, tools } = this;
        const { planner } = this;

        for (const tableName in tableSpecs) {
            const fieldList = tableSpecs[tableName];

            tools[tableName] = planner.makeTools(
                fieldList, fieldSpecs, inspector);
        }

        return this;
    }
};
