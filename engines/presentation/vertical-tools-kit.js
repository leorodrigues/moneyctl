
const ToolsKit = require('./tools-kit');
const VerticalTableFlow = require('./vertical-table-flow');

module.exports = class VerticalToolsKit extends ToolsKit {
    makeTableFlow() {
        return new VerticalTableFlow();
    }
};