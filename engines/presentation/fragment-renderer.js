const { Transform } = require('stream');

module.exports = class FragmentRenderer {
    render() {
        return new Transform({
            objectMode: true,
            transform(object, enc, callback) {
                this.push(fragmentToBuffer(object));
                callback();
            }
        });
    }

    stringify() {
        return new Transform({
            objectMode: true,
            transform(object, enc, callback) {
                this.push(JSON.stringify(object) + '\n');
                callback();
            }
        });
    }
};

function fragmentToBuffer(object) {
    if (object.control === 'newLine')
        return Buffer.from('\n');
    const {
        pad = 1, fill = ' ', align = 'left', value = '', style = [ ]
    } = object;
    const styleWidth = style.reduce((s,S) => s + S.length, 0);
    let { width, text } = object;
    text = text || value.toString();
    width = width || text.length;
    const buffer = Buffer.alloc(width + pad + styleWidth, fill);
    write(align, width, pad, buffer, text, style);
    return buffer;
}

function write(align, fieldLength, pad, buffer, text, style) {
    const contentLength = Math.min(text.length, fieldLength);
    let textOffset = computeOffset(contentLength, fieldLength, align);
    if (style.length) {
        const [start, end] = style;
        textOffset += buffer.write(start);
        textOffset += buffer.write(text, textOffset, contentLength);
        textOffset += buffer.write(end, fieldLength + start.length + pad);
        return textOffset;
    }
    return buffer.write(text, textOffset, fieldLength);
}

function computeOffset(length, width, align) {
    if (align === 'left') return 0;
    return width - length;
}