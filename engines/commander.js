const { Readable } = require('stream');

const pathToRegexp = require('path-to-regexp');

module.exports = class Commander {
    constructor(continuation) {
        this.commands = [ ];
        this.afterCommands = [ ];
        this.beforeCommands = [ ];
        this.continuation = continuation;
    }

    beforeEach(...handlers) {
        this.beforeCommands.push(...handlers);
    }

    afterEach(...handlers) {
        this.afterCommands.push(...handlers);
    }

    delete(path, ...handlers) {
        const regexp = pathToRegexp(path);
        this.commands.push({ method: 'delete', regexp, handlers, path });
    }

    post(path, ...handlers) {
        const regexp = pathToRegexp(path);
        this.commands.push({ method: 'post', regexp, handlers, path });
    }

    put(path, ...handlers) {
        const regexp = pathToRegexp(path);
        this.commands.push({ method: 'put', regexp, handlers, path });
    }

    get(path, ...handlers) {
        const regexp = pathToRegexp(path);
        this.commands.push({ method: 'get', regexp, handlers, path });
    }

    async dispatch(input) {
        const { beforeCommands, afterCommands } = this;
        const inputRegexp = /^(quit|[\w-]+)?(\s*(\/.*))?$/;
        if (!inputRegexp.test(input)) return true;

        const [,inputMethod = 'get',, inputPath] = inputRegexp.exec(input);
        if (inputMethod === 'quit') return false;

        const item = this.commands.find(matchingInput(inputMethod, inputPath));
        if (!item) return true;

        const { handlers, regexp: routeRegexp } = item;
        const params = buildArguments(inputPath, routeRegexp);
        const context = { params, commander: this };
        await this.continuation.follow(
            context, ...beforeCommands, ...handlers, ...afterCommands);
        disposeOfContext(context);
        return true;
    }

    streamHelp() {
        let index = 0;
        const { commands } = this;
        return new Readable({
            objectMode: true,
            read() {
                if (index >= commands.length) {
                    this.push(null);
                    return;
                }
                this.push(makeItem(commands[index]));
                index++;
            }
        });
    }
};

function makeItem({ path, method }) {
    return {
        *iterateFragments() {
            yield { frag: 'path', value: path };
            yield { frag: 'method', value: method };
        }
    };
}

function skiptFirst(k, i) {
    return i > 0;
}

function matchingInput(method, path) {
    return ({ method: routeMethod, regexp: routeRegexp }) => {
        return routeMethod === method && routeRegexp.test(path);
    };
}

function buildArguments(path, pathRegex) {
    const match = pathRegex.exec(path);
    const underKeyName = (p, c, i) => (p[pathRegex.keys[i].name] = c) && p;
    return match.filter(skiptFirst).reduce(underKeyName, {});
}

function disposeOfContext(context) {
    for (const name in context)
        delete context[name];
}