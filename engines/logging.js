const { Writable } = require('stream');

const log = require('fancy-log');

const { red, cyan, yellow } = require('chalk');

function error(message) {
    log.error(`${red('error')}: ${message}`);
}

function info(message) {
    log.info(`${cyan('info')}: ${message}`);
}

function warn(message) {
    log.warn(`${yellow('warn')}: ${message}`);
}

function print(text) {
    // eslint-disable-next-line no-console
    console.log(text);
}

function asyncPipeAndConsumeStream(stream) {
    return new Promise(resolve => {
        let buffer = Buffer.alloc(0);
        stream.pipe(new Writable({
            objectMode: true,
            write(chunk, enc, callback) {
                buffer = Buffer.concat([buffer, chunk]);
                if (flush(chunk)) {
                    print(buffer.slice(0, buffer.length - 1).toString());
                    buffer = Buffer.alloc(0);
                }
                callback();
            },
            final(callback) {
                if (buffer.length > 0)
                    print(buffer.toString());
                callback();
                resolve();
            }
        }));
    });
}

function flush(chunk) {
    return chunk[chunk.length - 1] === 10;
}

module.exports = { print, info, warn, error, asyncPipeAndConsumeStream };