const Emitter = require('events');

function copyOne(original, copy, emitter) {
    return name => {
        const update = newValue => copy[name] = newValue;
        emitter.emit(name, original[name], update);
        if (!(name in copy) && (name in original))
            update(original[name]);
    };
}

function assistedClonning(original, register, keys = [], copy = { }) {
    const emitter = new Emitter();
    if (register instanceof Function)
        register(emitter);

    const copyFn = copyOne(original, copy, emitter);

    if (keys.length > 0) {
        keys.forEach(copyFn);
    } else {
        Object.keys(original).forEach(copyFn);
    }

    return copy;
}

module.exports = { assistedClonning };