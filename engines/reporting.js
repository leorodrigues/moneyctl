
const { red, green } = require('chalk');

function reportError(message) {
    return `>> ${red(message)}`;
}

function reportAccountIdNotFound(accountId) {
    return reportError(`Account ${green(accountId)} not found.`);
}

function reportMovementIdNotFound(movementId) {
    return reportError(`Movement ${green(movementId)} not found.`);
}

function reportMovementLineIdNotFound(movementLineId) {
    return reportError(`Movement line ${green(movementLineId)} not found.`);
}

function reportRecurrencyIdNotFound(recurrencyId) {
    return reportError(`Recurrency ${green(recurrencyId)} not found.`);
}

function reportTransferIdNotFound(transferId) {
    return reportError(`Transfer ${green(transferId)} not found.`);
}

function reportBindingIdNotFound(bindingId) {
    return reportError(`Binding ${green(bindingId)} not found.`);
}

module.exports = {
    reportError,
    reportBindingIdNotFound,
    reportMovementLineIdNotFound,
    reportAccountIdNotFound,
    reportMovementIdNotFound,
    reportRecurrencyIdNotFound,
    reportTransferIdNotFound
};
