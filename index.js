const inquirer = require('inquirer');
const log = require('./engines/logging');
const Commander = require('./engines/commander');
const continuation = require('./engines/continuation');

const { IntroBanner } = require('./engines/presentation');

const Boot = require('./app/boot');

const inquirerPrompt = inquirer.createPromptModule();

inquirerPrompt.registerPrompt('command',
    require('inquirer-command-prompt'));

inquirerPrompt.registerPrompt('datetime',
    require('inquirer-datepicker-prompt'));

const boot = new Boot();

const commander = new Commander(continuation);

boot.run(commander, inquirerPrompt);

const COMMAND_PROMPT = {
    'type': 'command',
    'name': 'cmdLine',
    'constext': 0,
    'short': false,
    'message': ':'
};

async function handleSuccess(context) {
    try {
        const { cmdLine } = context;
        const keepRunning = await commander.dispatch(cmdLine);
        if (keepRunning)
            return await handleSuccess(await inquirerPrompt(COMMAND_PROMPT));
        log.print('\nPrompt cycle terminated.');
    } catch(error) {
        return await handleError(error);
    }
}

async function handleError(error) {
    log.error(error.message);
    try {
        return await handleSuccess(await inquirerPrompt(COMMAND_PROMPT));
    } catch(error) {
        return await handleError(error);
    }
}

const intro = new IntroBanner();

intro.asyncOutputBanner(require('./package.json'), process.stdout).then(() => {
    inquirerPrompt(COMMAND_PROMPT).then(handleSuccess).catch(handleError);
});
