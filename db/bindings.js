const {
    withCollection,
    collectDocuments,
    streamDocuments,
    insertOne,
    updateOne,
    remove
} = require('./support');

const { Binding } = require('../engines/model');

async function deleteById(id) {
    const bindings = await withCollection('bindings');
    return await remove(bindings, { _id: parseInt(id) });
}

async function findById(id) {
    const bindings = await withCollection('bindings');
    const result = await collectDocuments(
        bindings.find({ _id: id }), Binding.desserialize);
    return result.length ? result[0] : null;
}

async function findAll(query = {}) {
    const bindings = await withCollection('bindings');
    const result = await collectDocuments(
        bindings.find(query), Binding.desserialize);
    return result;
}

async function insert(data) {
    const bindings = await withCollection('bindings');
    return Binding.desserialize(await insertOne(bindings, data.serializable));
}

async function update(data) {
    const query = { _id: data._id };
    const bindings = await withCollection('bindings');
    return await updateOne(bindings, query, data.serializable);
}

async function streamAll() {
    const bindings = await withCollection('bindings');
    return streamDocuments(bindings.find(), Binding.desserialize);
}

module.exports = {
    insert,
    update,
    findAll,
    findById,
    streamAll,
    deleteById
};
