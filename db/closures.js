const {
    withCollection,
    collectDocuments,
    streamDocuments,
    insertOne,
    updateOne,
    upsertOne,
    remove
} = require('./support');

const { Closure } = require('../engines/model');

async function deleteById(id) {
    const closures = await withCollection('closures');
    return await remove(closures, { _id: parseInt(id) });
}

async function deleteInstance(data) {
    const closures = await withCollection('closures');
    return await remove(closures, { _id: data.serializable._id });
}

async function findById(id) {
    const closures = await withCollection('closures');
    const result = await collectDocuments(
        closures.find({ _id: id }), Closure.desserialize);
    return result.length ? result[0] : null;
}

async function findAll(query = {}) {
    const closures = await withCollection('closures');
    const result = await collectDocuments(
        closures.find(query), Closure.desserialize);
    return result;
}

async function insert(data) {
    const closures = await withCollection('closures');
    const result = await insertOne(closures, data.serializable);
    return Closure.desserialize(result);
}

async function update(data) {
    const query = { _id: data._id };
    const closures = await withCollection('closures');
    return await updateOne(closures, query, data.serializable);
}

async function save(data) {
    const query = { _id: data._id };
    const closures = await withCollection('closures');
    return await upsertOne(closures, query, data.serializable);
}

async function streamAll() {
    const options = { sort: 'order' };
    const closures = await withCollection('closures');
    return streamDocuments(closures.find({}, options), Closure.desserialize);
}

module.exports = {
    streamAll,
    insert,
    update,
    deleteInstance,
    findById,
    findAll,
    deleteById,
    save
};
