const {
    withCollection,
    collectDocuments,
    streamDocuments,
    insertOne,
    updateOne,
    remove
} = require('./support');

const { Recurrency } = require('../engines/model');

async function deleteById(id) {
    const recurrencies = await withCollection('recurrencies');
    return await remove(recurrencies, { _id: parseInt(id) });
}

async function deleteInstance({ _id }) {
    const recurrencies = await withCollection('recurrencies');
    return await remove(recurrencies, { _id });
}

async function findById(id) {
    const recurrencies = await withCollection('recurrencies');
    const result = await collectDocuments(
        recurrencies.find({ _id: id }), Recurrency.desserialize);
    return result.length ? result[0] : null;
}

async function findAll(query = {}) {
    const recurrencies = await withCollection('recurrencies');
    return await collectDocuments(
        recurrencies.find(query), Recurrency.desserialize);
}

async function insert(data) {
    const recurrencies = await withCollection('recurrencies');
    return Recurrency.desserialize(
        await insertOne(recurrencies, data.serializable));
}

async function update(data) {
    const query = { _id: data._id };
    const recurrencies = await withCollection('recurrencies');
    return await updateOne(recurrencies, query, data.serializable);
}

async function streamAll() {
    const recurrencies = await withCollection('recurrencies');
    return streamDocuments(recurrencies.find(), Recurrency.desserialize);
}

module.exports = {
    insert,
    update,
    findById,
    findAll,
    deleteById,
    deleteInstance,
    streamAll
};
