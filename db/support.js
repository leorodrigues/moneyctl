const { Readable } = require('stream');
const Engine = require('tingodb')({ searchInArray: true });
const path = require('path');
const fs = require('fs');

const DATA_PATH = process.env.MONEYCTL_DATA_PATH
    || process.env.HOME
    || process.env.PWD
    || process.env.cwd();

const PATH = path.resolve(DATA_PATH, '.moneyctl', 'data');

if (!fs.existsSync(PATH))
    fs.mkdirSync(PATH, { recursive: true });

const DB = new Engine.Db(PATH, {});

class DatabaseError extends Error {
    constructor(message, cause) {
        super(message + ' - ' + cause.message);
        this.cause = cause;
        this.stack = `${this.stack}\n\n--- CAUSED BY ---\n\n${cause.stack}`;
    }
}

function collectRaw(cursor) {
    return new Promise((resolve, reject) => {
        cursor.toArray((error, object) => {
            if (error)
                reject(error);
            else
                resolve(object);
        });
    });
}

function collectTransformed(cursor, transform) {
    return new Promise((resolve, reject) => {
        cursor.toArray((error, object) => {
            if (error)
                reject(error);
            else
                resolve(object.map(transform));
        });
    });
}

async function collectDocuments(cursor, transform) {
    if (transform && transform instanceof Function)
        return await collectTransformed(cursor, transform);
    return await collectRaw(cursor);
}

function streamDocuments(cursor, transform) {
    return new Readable({
        objectMode: true,
        read() {
            cursor.nextObject((error, object) => {
                if (error) throw error;
                if (transform && object)
                    object = transform(object);
                this.push(object);
            });
        }
    });
}

function insertOne(collection, data) {
    return new Promise((resolve, reject) => {
        collection.insert(data, (error, result) => {
            if (error)
                reject(error);
            else
                resolve(result[0]);
        });
    });
}


function updateOne(collection, query, data) {
    return new Promise((resolve, reject) => {
        collection.update(query, data, (error, count) => {
            if (error)
                reject(error);
            else
                resolve(count);
        });
    });
}

function upsertOne(collection, query, data) {
    return new Promise((resolve, reject) => {
        collection.update(query, data, { upsert: true }, (error, count) => {
            if (error)
                reject(error);
            else
                resolve(count);
        });
    });
}

function remove(collection, query) {
    return new Promise((resolve, reject) => {
        collection.remove(query, (error, count) => {
            if (error)
                reject(error);
            else
                resolve(count);
        });
    });
}

function dropCollectionByName(name) {
    return new Promise((resolve, reject) => {
        withCollection(name).then(collection => {
            collection.drop((error, result) => {
                if (error)
                    reject(error);
                else
                    resolve(result);
            });
        });
    });
}

function withCollection(collectionName) {
    return new Promise((resolve, reject) => {
        DB.collection(collectionName, (error, collection) => {
            if (error)
                reject(error);
            else
                resolve(collection);
        });
    });
}

function exists(collection, query) {
    return new Promise((resolve, reject) => {
        collection.findOne(query, (error, doc) => {
            if (error)
                reject(error);
            else
                resolve(doc !== null);
        });
    });
}

module.exports = {
    dropCollectionByName,
    collectDocuments,
    streamDocuments,
    withCollection,
    DatabaseError,
    insertOne,
    updateOne,
    upsertOne,
    exists,
    remove
};
