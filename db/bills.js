const {
    withCollection,
    collectDocuments,
    streamDocuments,
    insertOne,
    updateOne,
    exists,
    remove
} = require('./support');

const { Bill } = require('../engines/model');

async function deleteById(id) {
    const bills = await withCollection('bills');
    return await remove(bills, { _id: parseInt(id) });
}

async function findById(id) {
    const bills = await withCollection('bills');
    const result = await collectDocuments(
        bills.find({ _id: id }), Bill.desserialize);
    return result.length ? result[0] : null;
}

async function findAll(query = {}) {
    const bills = await withCollection('bills');
    const result = await collectDocuments(
        bills.find(query), Bill.desserialize);
    return result;
}

async function insert(data) {
    const bills = await withCollection('bills');
    const result = await insertOne(bills, data.serializable);
    return Bill.desserialize(result);
}

async function update(data) {
    const query = { _id: data._id };
    const bills = await withCollection('bills');
    return await updateOne(bills, query, data.serializable);
}

async function streamAll() {
    const bills = await withCollection('bills');
    return streamDocuments(bills.find(), Bill.desserialize);
}

async function existsOpenByAccountToken(accountToken) {
    const bills = await withCollection('bills');
    return await exists(bills, { accountToken, closed: false });
}

module.exports = {
    streamAll,
    insert,
    update,
    findById,
    findAll,
    deleteById,
    existsOpenByAccountToken
};
