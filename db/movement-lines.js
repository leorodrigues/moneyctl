const {
    withCollection,
    streamDocuments,
    collectDocuments,
    insertOne,
    updateOne,
    remove
} = require('./support');

const { MovementLine } = require('../engines/model');

async function deleteById(id) {
    const lines = await withCollection('movement-lines');
    return await remove(lines, { _id: parseInt(id) });
}

async function deleteInstance({ _id }) {
    const lines = await withCollection('movement-lines');
    return await remove(lines, { _id });
}

async function findById(id) {
    const lines = await withCollection('movement-lines');
    const result = await collectDocuments(
        lines.find({ _id: id }), MovementLine.desserialize);
    return result.length ? result[0] : null;
}

async function findIncomeById(id) {
    const lines = await withCollection('movement-lines');
    const result = await collectDocuments(
        lines.find({ _id: id, type: 'credit' }), MovementLine.desserialize);
    return result.length ? result[0] : null;
}

async function findExpenseById(id) {
    const lines = await withCollection('movement-lines');
    const result = await collectDocuments(
        lines.find({ _id: id, type: 'debit' }), MovementLine.desserialize);
    return result.length ? result[0] : null;
}

async function findAll(query = {}) {
    const lines = await withCollection('movement-lines');
    const result = await collectDocuments(
        lines.find(query), MovementLine.desserialize);
    return result;
}

async function findAllExpenses() {
    const lines = await withCollection('movement-lines');
    return await collectDocuments(
        lines.find({ type: 'debit' }), MovementLine.desserialize);
}

async function findAllIncomes() {
    const lines = await withCollection('movement-lines');
    return await collectDocuments(
        lines.find({ type: 'credit'}), MovementLine.desserialize);
}

async function insert(data) {
    const lines = await withCollection('movement-lines');
    const final = await insertOne(lines, data.serializable);
    return MovementLine.desserialize(final);
}

async function update(data) {
    const query = { _id: data._id };
    const lines = await withCollection('movement-lines');
    return await updateOne(lines, query, data.serializable);
}

async function streamAll() {
    const lines = await withCollection('movement-lines');
    return streamDocuments(lines.find(), MovementLine.desserialize);
}

async function streamAllExpenses() {
    const lines = await withCollection('movement-lines');
    return streamDocuments(
        lines.find({ type: 'debit' }), MovementLine.desserialize);
}

async function streamAllIncomes() {
    const lines = await withCollection('movement-lines');
    return streamDocuments(
        lines.find({ type: 'credit' }), MovementLine.desserialize);
}

async function streamAllActiveBetween(startDate, endDate) {
    const query = {
        $or: [
            { 'activity.startDate': { $lte: endDate.toDate() } },
            { 'activity.endDate': { $gte: startDate.toDate() } }
        ]
    };
    const lines = await withCollection('movement-lines');
    return streamDocuments(lines.find(query), MovementLine.desserialize);
}

module.exports = {
    insert,
    update,
    findById,
    findAll,
    findAllIncomes,
    findAllExpenses,
    findIncomeById,
    findExpenseById,
    deleteById,
    deleteInstance,
    streamAll,
    streamAllActiveBetween,
    streamAllExpenses,
    streamAllIncomes
};
