module.exports = {
    accounts: require('./accounts'),
    bills: require('./bills'),
    bindings: require('./bindings'),
    closures: require('./closures'),
    movementLines: require('./movement-lines'),
    movements: require('./movements'),
    recurrencies: require('./recurrencies'),
    transfers: require('./transfers')
};