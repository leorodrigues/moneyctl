const {
    withCollection,
    collectDocuments,
    streamDocuments,
    insertOne,
    updateOne,
    remove
} = require('./support');

const { Transfer } = require('../engines/model');

async function deleteById(id) {
    const transfers = await withCollection('transfers');
    return await remove(transfers, { _id: parseInt(id) });
}

async function deleteInstance(data) {
    const closures = await withCollection('transfers');
    return await remove(closures, { _id: data.serializable._id });
}

async function findById(id) {
    const transfers = await withCollection('transfers');
    const result = await collectDocuments(
        transfers.find({ _id: id }), Transfer.desserialize);
    return result.length ? result[0] : null;
}

async function findAll(query = {}) {
    const transfers = await withCollection('transfers');
    const result = await collectDocuments(
        transfers.find(query), Transfer.desserialize);
    return result;
}

async function insert(data) {
    const transfers = await withCollection('transfers');
    const result = await insertOne(transfers, data.serializable);
    return Transfer.desserialize(result);
}

async function update(data) {
    const query = { _id: data._id };
    const transfers = await withCollection('transfers');
    return await updateOne(transfers, query, data.serializable);
}

async function streamAll() {
    const transfers = await withCollection('transfers');
    return streamDocuments(transfers.find(), Transfer.desserialize);
}

module.exports = {
    streamAll,
    insert,
    update,
    findById,
    findAll,
    deleteById,
    deleteInstance
};
