const {
    withCollection,
    collectDocuments,
    streamDocuments,
    insertOne,
    updateOne,
    remove
} = require('./support');

const { Movement } = require('../engines/model');

async function findById(id) {
    const movements = await withCollection('movements');
    const result = await collectDocuments(
        movements.find({ _id: id }), Movement.desserialize);
    return result.length ? result[0] : null;
}

async function findAllByRecurrencyToken(recurrencyToken) {
    const movements = await withCollection('movements');
    const query = { recurrencyToken };
    const result = await collectDocuments(
        movements.find(query), Movement.desserialize);
    return result.length ? result : [];
}

async function findAllByLineToken(lineToken) {
    const movements = await withCollection('movements');
    const query = { lineToken };
    const result = await collectDocuments(
        movements.find(query), Movement.desserialize);
    return result.length ? result : [];
}

async function findAllExpenses() {
    const movements = await withCollection('movements');
    const result = await collectDocuments(
        movements.find({ type: 'debit' }), Movement.desserialize);
    return result.length ? result : [];
}

async function findAllIncomes() {
    const movements = await withCollection('movements');
    const result = await collectDocuments(
        movements.find({ type: 'credit' }), Movement.desserialize);
    return result.length ? result : [];
}

async function findAll(query = {}) {
    const movements = await withCollection('movements');
    const result = await collectDocuments(
        movements.find(query), Movement.desserialize);
    return result.length ? result : [];
}

async function streamAllIncomes() {
    const movements = await withCollection('movements');
    return streamDocuments(
        movements.find({ type: 'credit' }), Movement.desserialize);
}

async function streamAllExpenses() {
    const movements = await withCollection('movements');
    return streamDocuments(
        movements.find({ type: 'debit' }), Movement.desserialize);
}

async function streamAllByYearAndMonth(referenceDate) {
    const startDate = referenceDate.startOf('month').toDate();
    const endDate = referenceDate.endOf('month').toDate();
    const movements = await withCollection('movements');
    return streamDocuments(movements.find({
        operationDate: { $lte: endDate, $gte: startDate }
    }), Movement.desserialize);
}

async function streamAllByDateInterval(startDate, endDate) {
    const movements = await withCollection('movements');
    return streamDocuments(movements.find({
        operationDate: { $lte: endDate.toDate(), $gte: startDate.toDate() }
    }), Movement.desserialize);
}

async function streamAllEffectiveBetweenDates(startDate, endDate) {
    const inInterval = { $lte: endDate.toDate(), $gte: startDate.toDate() };
    const movements = await withCollection('movements');
    return streamDocuments(movements.find({
        $or: [ { operationDate: inInterval }, { billingDate: inInterval } ]
    }), Movement.desserialize);
}

async function insert(data) {
    const movements = await withCollection('movements');
    const result = await insertOne(movements, data.serializable);
    return Movement.desserialize(result);
}

async function update(data) {
    const query = { _id: data._id };
    const movements = await withCollection('movements');
    return await updateOne(movements, query, data.serializable);
}

async function deleteById(id) {
    const movements = await withCollection('movements');
    return await remove(movements, { _id: parseInt(id) });
}

async function deleteByLineToken(token) {
    const movements = await withCollection('movements');
    return await remove(movements, { lineToken: token });
}

async function deleteByRecurrencyToken(token) {
    const movements = await withCollection('movements');
    return await remove(movements, { recurrencyToken: token });
}

async function deleteByTransferToken(token) {
    const movements = await withCollection('movements');
    return await remove(movements, { transferToken: token });
}

module.exports = {
    insert,
    update,
    deleteById,
    deleteByLineToken,
    deleteByTransferToken,
    deleteByRecurrencyToken,
    findById,
    findAll,
    findAllByLineToken,
    findAllByRecurrencyToken,
    findAllExpenses,
    findAllIncomes,
    streamAllByYearAndMonth,
    streamAllByDateInterval,
    streamAllEffectiveBetweenDates,
    streamAllIncomes,
    streamAllExpenses
};
