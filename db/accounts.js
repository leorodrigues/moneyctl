const {
    withCollection,
    collectDocuments,
    streamDocuments,
    insertOne,
    updateOne,
    remove
} = require('./support');

const { Account } = require('../engines/model');

async function deleteById(id) {
    const accounts = await withCollection('accounts');
    return await remove(accounts, { _id: parseInt(id) });
}

async function findById(id) {
    const accounts = await withCollection('accounts');
    const result = await collectDocuments(
        accounts.find({ _id: id }), Account.desserialize);
    return result.length ? result[0] : null;
}

async function findByToken(token) {
    const accounts = await withCollection('accounts');
    const result = await collectDocuments(
        accounts.find({ token }), Account.desserialize);
    return result.length ? result[0] : null;
}

async function findAll(query = {}) {
    const accounts = await withCollection('accounts');
    const result = await collectDocuments(
        accounts.find(query), Account.desserialize);
    return result;
}

async function insert(data) {
    const accounts = await withCollection('accounts');
    const result = await insertOne(accounts, data.serializable);
    return Account.desserialize(result);
}

async function update(data) {
    const query = { _id: data._id };
    const accounts = await withCollection('accounts');
    return await updateOne(accounts, query, data.serializable);
}

async function streamAll() {
    const accounts = await withCollection('accounts');
    return streamDocuments(accounts.find(), Account.desserialize);
}

module.exports = {
    streamAll,
    insert,
    update,
    findById,
    findByToken,
    findAll,
    deleteById
};
