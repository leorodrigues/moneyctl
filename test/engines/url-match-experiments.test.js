const chai = require('chai');
const { expect } = chai;

const pathToRegexp = require('path-to-regexp');

describe('URL matching experiments', () => {
    const path = '/base/path/:first(a|b)/:second(\\d+)-:third(\\d+)';
    const regex = pathToRegexp(path);

    afterEach(() => regex.lastIndex = 0);

    it('Should have positional keys', () => {
        expect(regex.keys.map(k => k.name))
            .to.be.deep.equal(['first', 'second', 'third']);
    });

    it('Should parse url "/base/path/a/10-12"', () => {
        const match = regex.exec('/base/path/a/10-12');
        expect(match.map(x => x)).to.be.deep.equal([
            '/base/path/a/10-12',
            'a',
            '10',
            '12'
        ]);
    });
});