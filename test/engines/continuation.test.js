const chai = require('chai');
const sinon = require('sinon');

const {follow} = require('../../engines/continuation');

chai.use(require('sinon-chai'));
const {expect} = chai;

const sandbox = sinon.createSandbox();
const handlerStub = sandbox.stub();

const handle = (context, next) => handlerStub(context) || next();

describe('lib/engines/continuation', () => {
    afterEach(() => sandbox.reset());

    describe('#follow(context, ...handlers)', () => {
        it('Should activate the handler once', async () => {
            await follow('context', handle);
            expect(handlerStub).to.be.calledOnceWith('context');
        });
        it('Should activate the handler twice', async () => {
            await follow('context', handle, handle);
            expect(handlerStub).to.be.calledTwice.and.calledWith('context');
        });
        it('Should break the activation chain', async () => {
            handlerStub.returns(true);
            await follow('context', handle, handle);
            expect(handlerStub).to.be.calledOnce.and.calledWith('context');
        });
    });
});