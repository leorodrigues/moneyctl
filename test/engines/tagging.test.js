const chai = require('chai');
const { expandTags } = require('../../engines/model');

const {expect} = chai;

describe('lib/engines/model', () => {
    describe('#expandTags(tagsString)', () => {
        it('Should expand into a map', () => {
            const input = 'account: 1234; description: some long text;';
            expect(expandTags(input)).to.be.deep.equal({
                account: '1234',
                description: 'some long text',
                predicates: []
            });
        });
        it('Should expand into a list', () => {
            const input = 'begin; end; start;stop;';
            expect(expandTags(input)).to.be.deep.equal({
                predicates: ['begin', 'end', 'start', 'stop']
            });
        });
        it('Should expand to empty object', () => {
            expect(expandTags(';')).to.be.deep.equal({ predicates: [] });
        });
    });
});
