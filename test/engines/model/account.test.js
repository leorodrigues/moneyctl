const moment = require('moment');

const chai = require('chai');
const { Account } = require('../../../engines/model');

const { expect } = chai;

function fixedOffsetMoment(momentString) {
    const result = moment(momentString).startOf('date');
    result.utcOffset(-180);
    return result;
}

describe('model/Account', () => {
    describe('#computeBillingDate(billOpeningDate)', () => {
        it('Should jump one week', () => {
            const subject = new Account({ billing: {
                period: 'week', periodLength: 1
            }});
            const openingDate = fixedOffsetMoment('2019-04-14');
            const result = subject.computeBillingDate(openingDate);
            expect(result.toString())
                .to.be.equal('Sun Apr 21 2019 23:59:59 GMT-0300');
        });
        it('Should jump two weeks', () => {
            const subject = new Account({ billing: {
                period: 'week', periodLength: 2
            }});
            const openingDate = fixedOffsetMoment('2019-04-14');
            const result = subject.computeBillingDate(openingDate);
            expect(result.toString())
                .to.be.equal('Sun Apr 28 2019 23:59:59 GMT-0300');
        });
        it('Should jump a month, but choose day 3', () => {
            const subject = new Account({ billing: {
                period: 'month',
                periodLength: 1,
                closureDay: 3,
                isExtraPeriod: true
            }});
            const openingDate = fixedOffsetMoment('2019-04-14');
            const result = subject.computeBillingDate(openingDate);
            expect(result.toString())
                .to.be.equal('Fri May 03 2019 23:59:59 GMT-0300');
        });
        it('Should jump two months, but choose day 17', () => {
            const subject = new Account({ billing: {
                period: 'month',
                periodLength: 2,
                closureDay: 17,
                isExtraPeriod: true
            }});
            const openingDate = fixedOffsetMoment('2019-04-14');
            const result = subject.computeBillingDate(openingDate);
            expect(result.toString())
                .to.be.equal('Mon Jun 17 2019 23:59:59 GMT-0300');
        });
        it('Should jump two months because it is intra-period', () => {
            const subject = new Account({ billing: {
                period: 'month',
                periodLength: 3,
                closureDay: 17,
                isExtraPeriod: false
            }});
            const openingDate = fixedOffsetMoment('2019-04-14');
            const result = subject.computeBillingDate(openingDate);
            expect(result.toString())
                .to.be.equal('Mon Jun 17 2019 23:59:59 GMT-0300');
        });
    });
});