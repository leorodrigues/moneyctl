const { Readable } = require('stream');
const chai = require('chai');
const { expect } = chai;

const { ClosureSequencer, Factory } = require('../../../engines/model');

const factory = new Factory();

const accountA = factory.makeCurrentAccount({ name: 'A', openingBalance: 0 });
const accountB = factory.makeCurrentAccount({ name: 'B', openingBalance: 0 });
const accountC = factory.makeCurrentAccount({ name: 'C', openingBalance: 0 });
const accountD = factory.makeCurrentAccount({ name: 'D', openingBalance: 0 });
const accountE = factory.makeCurrentAccount({ name: 'E', openingBalance: 0 });
const accountX = factory.makeCurrentAccount({ name: 'X', openingBalance: 0 });

describe('engines/model/closure-sequencer', () => {
    it('Should compute the sequence B,A', async () => {
        const stream = streamArray([
            makeBinding('first', accountA, accountB)
        ]);
        const subject = new ClosureSequencer();
        await subject.asyncPipeAndPlaceBinding(stream);
        expect(subject.sequence).to.be.deep.equal(['b', 'a']);
    });
    it('Should compute the sequence B,A,C', async () => {
        const stream = streamArray([
            makeBinding('first', accountA, accountB),
            makeBinding('second', accountC, accountA)
        ]);
        const subject = new ClosureSequencer();
        await subject.asyncPipeAndPlaceBinding(stream);
        expect(subject.sequence).to.be.deep.equal(['b', 'a', 'c']);
    });
    it('Should compute the sequence B,D,A,C', async () => {
        const stream = streamArray([
            makeBinding('first', accountA, accountB),
            makeBinding('second', accountC, accountA),
            makeBinding('third', accountA, accountD)
        ]);
        const subject = new ClosureSequencer();
        await subject.asyncPipeAndPlaceBinding(stream);
        expect(subject.sequence).to.be.deep.equal(['b', 'd', 'a', 'c']);
    });
    it('Should compute the sequence E,B,D,A,C', async () => {
        const stream = streamArray([
            makeBinding('first', accountA, accountB),
            makeBinding('second', accountC, accountA),
            makeBinding('third', accountA, accountD),
            makeBinding('fourth', accountB, accountE)
        ]);
        const subject = new ClosureSequencer();
        await subject.asyncPipeAndPlaceBinding(stream);
        expect(subject.sequence).to.be.deep.equal(['e', 'b', 'd', 'a', 'c']);
    });
    it('Should compute the sequence E,B,X,D,A,C', async () => {
        const stream = streamArray([
            makeBinding('first', accountA, accountB),
            makeBinding('second', accountC, accountA),
            makeBinding('third', accountA, accountD),
            makeBinding('fourth', accountB, accountE),
            makeBinding('fifth', accountX, accountB),
        ]);
        const subject = new ClosureSequencer();
        await subject.asyncPipeAndPlaceBinding(stream);
        expect(subject.sequence).to.be.deep.equal(['e', 'b', 'x', 'd', 'a', 'c']);
    });
});

function makeBinding(name, draweeAccount, assigneeAccount) {
    return factory.makeBinding({
        name, tags: {}, assigneeAccount, draweeAccount
    });
}

function streamArray(array) {
    return new Readable({
        objectMode: true,
        read() {
            this.push(array.shift() || null);
        }
    });
}