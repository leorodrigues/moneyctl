const proxyquire = require('proxyquire');
const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const sandbox = sinon.createSandbox();

const accountConstructor = sandbox.stub();

const Factory = proxyquire('../../../engines/model/factory', {
    './account': function(a) { accountConstructor(a); }
});

const subject = new Factory();

describe('model/Factory', () => {
    afterEach(() => sandbox.reset());

    describe('#makeCurrentAccount(accountData)', () => {
        it('Should make an account', () => {
            subject.makeCurrentAccount({
                name: 'Some bank',
                openingBalance: 10
            });
            expect(accountConstructor)
                .to.be.calledOnceWith({
                    token: 'some-bank',
                    name: 'Some bank',
                    type: 'current',
                    openingBalance: 10
                });
        });
    });

    describe('#makeCreditAccount(accountData)', () => {
        it('Should make an account', () => {
            subject.makeCreditAccount({
                name: 'Some bank',
                openingBalance: 10,
                billingPeriod: 'month',
                billingPeriodLength: 1,
                billingClosureDay: 3,
                billingDeadlineDistance: 7,
                isExtraPeriod: false
            });
            expect(accountConstructor)
                .to.be.calledOnceWith({
                    name: 'Some bank',
                    type: 'credit',
                    token: 'some-bank',
                    openingBalance: 10,
                    billing: {
                        period: 'month',
                        periodLength: 1,
                        closureDay: 3,
                        deadlineDistance: 7,
                        isExtraPeriod: false
                    }
                });
        });
    });
});