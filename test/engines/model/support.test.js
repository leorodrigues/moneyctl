const chai = require('chai');
const moment = require('moment');
const { generateDateSequence } = require('../../../engines/model');

const { expect } = chai;

describe('engines/model/support', () => {
    describe('#generateDateSequence(startDate, size, period, periodLengh', () => {
        it('Should generate only one by month', () => {
            const start = moment('2019-01-23');
            const dates = [...generateDateSequence(start, 1, 'month', 1)];
            expect(dates.length).to.be.equal(1);
            expect(dates[0].toString())
                .to.be.equal('Wed Jan 23 2019 00:00:00 GMT-0200');
        });

        it('Should generate two by month', () => {
            const start = moment('2019-01-23');
            const dates = [ ...generateDateSequence(start, 2, 'month', 1) ];
            expect(dates.map(d => d.toString()))
                .to.be.deep.equal([
                    'Wed Jan 23 2019 00:00:00 GMT-0200',
                    'Sat Feb 23 2019 00:00:00 GMT-0300'
                ]);
        });
    });
});