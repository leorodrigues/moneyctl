const moment = require('moment');
const chai = require('chai');
const { Closure, Balance } = require('../../../engines/model');

const { expect } = chai;

describe('engines/model/Closure', () => {
    describe('#closeBalance(balance)', () => {
        it('Should generate a debit in a month forward', () => {
            const balance = makeDummyBalance();
            const subject = new Closure({
                drawees: [
                    { bindingToken: 'x', name: 'X', tags: {}, draweeToken: 'K' }
                ],
                assigneeAccountToken: 'A'
            });
            subject.closeBalance(balance);
            expect(balance.data).to.be.deep.equal(expenctedBalanceData);
        });
    });
});

function makeDummyBalance() {
    const b = new Balance(moment('2019-01-01'), moment('2019-03-31'));
    b.onAccount('A')
        .onDate(moment('2019-01-10'))
        .onMovementType('debit')
        .aggregate(1, {});
    return b;
}

const expenctedBalanceData = {
    'A': {
        '2019-01': {
            'balance': 0,
            'credit': {
                'lines': {
                    'x': 1
                },
                'tags': {},
                'total': 1
            },
            'debit': {
                'lines': {
                    'unforseen': 1
                },
                'tags': {},
                'total': 1
            },
            'netIncome': 0
        },
        '2019-02': {
            'balance': 0,
            'credit': {
                'lines': {},
                'tags': {},
                'total': 0
            },
            'debit': {
                'lines': {},
                'tags': {},
                'total': 0
            },
            'netIncome': 0
        },
        '2019-03': {
            'balance': 0,
            'credit': {
                'lines': {},
                'tags': {},
                'total': 0
            },
            'debit': {
                'lines': {},
                'tags': {},
                'total': 0
            },
            'netIncome': 0
        },
        'lineTokens': [ ]
    },
    'K': {
        '2019-02': {
            'credit': {
                'lines': {},
                'tags': {},
                'total': 0
            },
            'debit': {
                'lines': {
                    'x': 1
                },
                'tags': {},
                'total': 1
            },
            'netIncome': -1
        },
        'lineTokens': [ ]
    }
};