const {assistedClonning} = require('../../engines/clonning');
const chai = require('chai');
const expect = chai.expect;

describe('lib/engines/clonning', () => {
    describe('#assistedClonning(original, register, keys)', () => {
        describe('when NO specific keys are given', () => {
            const copy = assistedClonning({a: 1, b: 2}, events => {
                events.on('a', (originalValue, update) => update(5));
            });
            it('should replace an updated property', () => {
                expect(copy.a).to.be.equals(5);
            });
            it('should preserve an untouched property', () => {
                expect(copy.b).to.be.equals(2);
            });
        });

        describe('when specific keys ARE given', () => {
            const copy = assistedClonning({a: 1, b: 2}, events => {
                events.on('a', (originalValue, update) => update(3))
                    .on('c', (originalValue, update) => update(5));
            }, ['a', 'b', 'c', 'd']);
    
            it('should replace an updated property', () => {
                expect(copy.a).to.be.equals(3);
            });
            it('should preserve an untouched property', () => {
                expect(copy.b).to.be.equals(2);
            });
            it('should add a missing property in response to an event', () => {
                expect(copy.c).to.be.equals(5);
            });
            it('should do nothing if the original has no property with specified name', () => {
                expect(copy).to.not.haveOwnProperty('d');
            });
        });
    });
});