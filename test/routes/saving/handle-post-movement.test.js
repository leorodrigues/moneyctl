const chai = require('chai');
const sinon = require('sinon');

const handlePostMovement =
    require('../../../routes/saving/handle-post-movement');

const { expect } = chai;

chai.use(require('sinon-chai'));

const sandbox = sinon.createSandbox();
const next = sandbox.stub();
const addIncome = sandbox.stub();
const addExpense = sandbox.stub();

describe('handle-post-movement', () => {
    afterEach(() => sandbox.reset());

    describe('postIncome', () => {
        const subject = handlePostMovement('income');
        it('Delegate the process to the ATM instance.', async () => {
            addIncome.resolves('income');
            const context = { atm: { addIncome }, answers: 'data' };
            await subject(context, next);
            expect(next).to.be.calledOnce;
            expect(context.incomes).to.be.deep.equal(['income']);
            expect(addIncome).to.be.calledOnceWithExactly('data');
        });
    });

    describe('postExpense', () => {
        const subject = handlePostMovement('expense');
        it('Delegate the process to the ATM instance.', async () => {
            addExpense.resolves('expense');
            const context = { atm: { addExpense }, answers: 'data' };
            await subject(context, next);
            expect(next).to.be.calledOnce;
            expect(context.expenses).to.be.deep.equal(['expense']);
            expect(addExpense).to.be.calledOnceWithExactly('data');
        });
    });
});