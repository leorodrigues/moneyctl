const chai = require('chai');
const { expect } = chai;

const HierarchyInspector =
    require('../../../engines/presentation/hierarchy-inspector');

const subject = new HierarchyInspector();

const data = {
    'horizontal.head.transform': 'upperCase',
    'vertical.label.width': 8,
    'vertical.cell.width': 10,
    'vertical.cell.align': 'right',
    'id.horizontal.width': 6,
    'name.horizontal.width': 12,
    'opening.cell.transform': 'twoDecimals',
    'opening.horizontal.width': 10,
    'opening.horizontal.cell.align': 'right',
    'type.horizontal.width': 8,
    'token.horizontal.width': 12,
    'drawee.horizontal.width': 12,
    'assignee.horizontal.width': 12,
    'account.horizontal.width': 12,
    'tags.horizontal.width': 20,
    'tags.cell.transform': 'flattenTags',
    'operationDate.horizontal.width': 10,
    'operationDate.cell.transform': 'isoDate',
    'operationDate.transform': 'OP. DATE',
    'amount.horizontal.width': 12,
    'amount.horizontal.cell.align': 'right',
    'amount.cell.transform': 'twoDecimals',
    'currency.horizontal.width': 3,
    'times.transform': 'PERIOD',
    'times.cell.transform': 'explainTime',
    'times.horizontal.width': 6,
    'periodLength.transform': ' ',
    'periodLength.cell.transform': 'explainPeriodLength',
    'periodLength.horizontal.width': 6,
    'period.transform': ' ',
    'period.cell.transform': 'explainPeriod',
    'period.horizontal.width': 6
};

describe('HierarchyInspector', () => {
    describe('#inspect(fieldSpecs, fieldName)', () => {
        it('Should return from an exact match', () => {
            const id = subject.inspect(data, 'id');
            expect(id.horizontal.cell.width.value).to.be.equal(6);
        });
        it('Should return from general default value', () => {
            const amount = subject.inspect(data, 'amount');
            expect(amount.horizontal.cell.align.value).to.be.equal('right');
        });

        it('Should return opening.horizontal.cell.transform', () => {
            const opening = subject.inspect(data, 'opening');
            expect(opening.horizontal.cell.transform.value).to.be.equal('twoDecimals');
        });

        it('Should return opening.vertical.cell.transform', () => {
            const opening = subject.inspect(data, 'opening');
            expect(opening.vertical.cell.transform.value).to.be.equal('twoDecimals');
        });

        it('Should return opening.horizontal.cell.align', () => {
            const opening = subject.inspect(data, 'opening');
            expect(opening.horizontal.cell.align.value).to.be.equal('right');
        });

        it('Should return opening.vertical.cell.align', () => {
            const opening = subject.inspect(data, 'opening');
            expect(opening.vertical.cell.align.value).to.be.equal('right');
        });
    });
});
