const {Writable} = require('stream');


function collectObjects(resolve) {
    const collection = [];
    return new Writable({
        objectMode: true,
        write(token, encoding, callback) {
            collection.push(token);
            callback();
        },
        final(callback) {
            callback();
            resolve(collection);
        }
    });
}

module.exports = {collectObjects};
